﻿using System.Linq;
using System.Windows.Forms;

namespace TexiDriverSystem
{
    public partial class FrmDriverTrainings : Form
    {
        public FrmDriverTrainings()
        {
            InitializeComponent();
        }

        private void FrmDriverTrainings_Load(object sender, System.EventArgs e)
        {
            // TODO: This line of code loads data into the 'taxi_Driver_SystemDataSet.Trainings' table. You can move, or remove it, as needed.
            this.trainingsTableAdapter.Fill(this.taxi_Driver_SystemDataSet.Trainings);

        }

        private void btnQua_Click(object sender, System.EventArgs e)
        {
            using (var db = new Taxi_Driver_SystemEntities())
            {
                if (btnQua.Text == @"Update Trainings")
                {
                    var row = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    var id = int.Parse(row);
                    var queryable = db.Trainings.FirstOrDefault(trainings => trainings.Id.Equals(id));
                    if (queryable != null)
                    {
                        queryable.CourseName = txtCourse.Text;
                        queryable.Description = txtDes.Text;   
                    }
                }
                else
                {
                    var list = db.Trainings.ToList();
                    var go = new Trainings
                    {
                        Id = list.Count + 1,
                        CourseName = txtCourse.Text,
                        Description = txtDes.Text
                    };
                    db.Trainings.Add(go);

                }
                db.SaveChanges();
            }

            trainingsTableAdapter.Fill(taxi_Driver_SystemDataSet.Trainings);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            btnQua.Text = @"Update Trainings";
            var row = dataGridView1.SelectedRows[0].Cells;
            txtCourse.Text = row[1].Value.ToString();
            txtDes.Text = row[2].Value.ToString();
        }
    }
}

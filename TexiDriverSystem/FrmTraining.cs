﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace TexiDriverSystem
{
    public partial class FrmTraining : Form
    {
        public FrmTraining()
        {
            InitializeComponent();
            OnLoad();
        }

        private void OnLoad()
        {
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                CmbDrivers.DataSource = db.Drivers.ToList();
                CmbDrivers.ValueMember = "Id";
                CmbDrivers.DisplayMember = "FirstName";
                CmbTrainings.DataSource = db.Trainings.ToList();
                CmbTrainings.ValueMember = "Id";
                CmbTrainings.DisplayMember = "CourseName";
            }
        }

        private void BtnSave_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSession.Text))
            {
                LblError.Text = "Enter Session Name";
                return;
            }
            TrainingSessions trainingSessions = new TrainingSessions();
            trainingSessions.DriverId = Convert.ToInt32(CmbDrivers.SelectedValue);
            trainingSessions.TrainingId = Convert.ToInt32(CmbTrainings.SelectedValue);
            trainingSessions.SessionName = TxtSession.Text;
            trainingSessions.SessionStartDate = DtpSessionStart.Value;
            trainingSessions.Status = 0;
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                var newId = db.TrainingSessions.OrderByDescending(r => r.Id).FirstOrDefault();
                trainingSessions.Id = newId == null ? 1 : newId.Id + 1;
                db.TrainingSessions.Add(trainingSessions);
                db.SaveChanges();
            }
            MessageBox.Show("Data Saved Successfully");
        }
    }
}

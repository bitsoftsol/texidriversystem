﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TexiDriverSystem.View_Models;

namespace TexiDriverSystem
{
    public partial class FrmDriverExpiryView : Form
    {
        public FrmDriverExpiryView()
        {
            InitializeComponent();
            OnLoad();
        }

        private void OnLoad()
        {
            var driverTrainings = new List<DriverTrainings>();
            var driverQualifications = new List<DriverQualifications>();
            var driverTrainingVm = new List<DriverTrainingVm>();
            var driverQualificationVm = new List<DriverQualificationsVm>();
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                var endDay = DateTime.Now.Date;
                var startDay = DateTime.Now.AddDays(-30);
                driverQualifications = db.DriverQualifications.Where(r => r.QualificationExpiryDate >= startDay && r.QualificationExpiryDate <= endDay).ToList();
                driverTrainings = db.DriverTrainings.Where(r => r.TrainingExpiryDate >= startDay && r.TrainingExpiryDate <= endDay).ToList();
                for (int i = 0; i < driverQualifications.Count; i++)
                {

                    var qualificationInsert = new DriverQualificationsVm();
                    qualificationInsert.Id = driverQualifications[i].Id;
                    qualificationInsert.FirstName = db.Drivers.SingleOrDefault(r => r.Id == driverQualifications[i].DriverId).FirstName;
                    qualificationInsert.LastName = db.Drivers.SingleOrDefault(r => r.Id == driverQualifications[i].DriverId).LastName;
                    qualificationInsert.LicenseNumber = driverQualifications[i].LicenseNumber;
                    qualificationInsert.GeographicalTests = driverQualifications[i].GeographicalTests;
                    qualificationInsert.QualificationExpiryDate = driverQualifications[i].QualificationExpiryDate;
                    qualificationInsert.QualificationName = db.Qualifications.SingleOrDefault(r => r.Id == driverQualifications[i].QualificationId) != null ? db.Qualifications.SingleOrDefault(r => r.Id == driverQualifications[i].QualificationId).QualificationName : null;
                    qualificationInsert.QualificationType = driverQualifications[i].QualificationType == 1 ? "License" : "Geographical Tests";
                    driverQualificationVm.Insert(i, qualificationInsert);
                }
                for (int j = 0; j < driverTrainings.Count; j++)
                {
                    var trainingInsert = new DriverTrainingVm();
                    trainingInsert.Id = driverTrainings[j].Id;
                    trainingInsert.TrainingExpiryDate = driverTrainings[j].TrainingExpiryDate;
                    trainingInsert.TrainingName = db.Trainings.SingleOrDefault(r => r.Id == driverTrainings[j].TrainingId) == null ? db.Trainings.SingleOrDefault(r => r.Id == driverTrainings[j].TrainingId).CourseName : null;
                    var sessions = db.TrainingSessions.SingleOrDefault(r => r.Id == driverTrainings[j].DriverSessionTrainingId);
                    trainingInsert.TrainingSessionName = sessions != null ? sessions.SessionName : null;
                    trainingInsert.TrainingSessionStatus = sessions.Status == 1 ? "Pass" : "Fail";
                    trainingInsert.FirstName = db.Drivers.SingleOrDefault(r => r.Id == driverQualifications[j].DriverId).FirstName;
                    trainingInsert.LastName = db.Drivers.SingleOrDefault(r => r.Id == driverQualifications[j].DriverId).LastName;
                    driverTrainingVm.Insert(j, trainingInsert);
                }
            }
            DgvQualificationDetails.AutoGenerateColumns = false;
            DgvQualificationDetails.DataSource = driverQualificationVm;
            DgvTrainingDetails.AutoGenerateColumns = false;
            DgvTrainingDetails.DataSource = driverTrainingVm;
        }
    }
}

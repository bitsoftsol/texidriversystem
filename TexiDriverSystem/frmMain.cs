﻿using System.Windows.Forms;
using TexiDriverSystem.Controls;

namespace TexiDriverSystem
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
        public void showControl(Control control)
        {
            if (!pnlControl.Controls.Contains(control))
            {
                pnlControl.Controls.Add(control);
                control.Dock = DockStyle.Fill;
                control.BringToFront();
                control.Focus();
            }
            else
            {
                control.BringToFront();
            }
        }

        private void btnTrainings_Click(object sender, System.EventArgs e)
        {
            showControl(TrainingControl.Instance);
        }

        private void btnDrivers_Click(object sender, System.EventArgs e)
        {
            showControl(DriverControl.Instance);
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            showControl(QualificationControl.Instance);
        }

        private void BtnTraining_Click(object sender, System.EventArgs e)
        {
            showControl(DriverControl.Instance);
        }

        private void BtnJourneyFeedback_Click(object sender, System.EventArgs e)
        {
            showControl(JourneyFeedback.Instance);
        }
    }
}

﻿namespace TexiDriverSystem
{
    partial class FrmDriverExpiryView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DgvQualificationDetails = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.DgvTrainingDetails = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualificationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualificationType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LicenseNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GeographicalTests = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualificationExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DriverFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DriverLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingSessionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DgvQualificationDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTrainingDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvQualificationDetails
            // 
            this.DgvQualificationDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvQualificationDetails.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvQualificationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvQualificationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.FirstName,
            this.LastName,
            this.QualificationName,
            this.QualificationType,
            this.LicenseNumber,
            this.GeographicalTests,
            this.QualificationExpiryDate});
            this.DgvQualificationDetails.Location = new System.Drawing.Point(3, 40);
            this.DgvQualificationDetails.Name = "DgvQualificationDetails";
            this.DgvQualificationDetails.Size = new System.Drawing.Size(794, 169);
            this.DgvQualificationDetails.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(593, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "List Of Drivers Who have Qualification Expiring within the next 30 Days";
            // 
            // DgvTrainingDetails
            // 
            this.DgvTrainingDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvTrainingDetails.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvTrainingDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTrainingDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TId,
            this.DriverFirstName,
            this.DriverLastName,
            this.TrainingName,
            this.TrainingSessionName,
            this.TrainingExpiryDate});
            this.DgvTrainingDetails.Location = new System.Drawing.Point(3, 243);
            this.DgvTrainingDetails.Name = "DgvTrainingDetails";
            this.DgvTrainingDetails.Size = new System.Drawing.Size(794, 169);
            this.DgvTrainingDetails.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-1, 212);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(560, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "List Of Drivers Who have Training Expiring within the next 30 Days";
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "First Name";
            this.FirstName.Name = "FirstName";
            // 
            // LastName
            // 
            this.LastName.DataPropertyName = "LastName";
            this.LastName.HeaderText = "Last Name";
            this.LastName.Name = "LastName";
            // 
            // QualificationName
            // 
            this.QualificationName.DataPropertyName = "QualificationName";
            this.QualificationName.HeaderText = "Qualification Name";
            this.QualificationName.Name = "QualificationName";
            // 
            // QualificationType
            // 
            this.QualificationType.DataPropertyName = "QualificationType";
            this.QualificationType.HeaderText = "Qualification Type";
            this.QualificationType.Name = "QualificationType";
            // 
            // LicenseNumber
            // 
            this.LicenseNumber.DataPropertyName = "LicenseNumber";
            this.LicenseNumber.HeaderText = "License Number";
            this.LicenseNumber.Name = "LicenseNumber";
            // 
            // GeographicalTests
            // 
            this.GeographicalTests.DataPropertyName = "GeographicalTests";
            this.GeographicalTests.HeaderText = "Geographical Tests";
            this.GeographicalTests.Name = "GeographicalTests";
            // 
            // QualificationExpiryDate
            // 
            this.QualificationExpiryDate.DataPropertyName = "QualificationExpiryDate";
            this.QualificationExpiryDate.HeaderText = "Qualification Expiry Date";
            this.QualificationExpiryDate.Name = "QualificationExpiryDate";
            // 
            // TId
            // 
            this.TId.DataPropertyName = "Id";
            this.TId.HeaderText = "Id";
            this.TId.Name = "TId";
            // 
            // DriverFirstName
            // 
            this.DriverFirstName.DataPropertyName = "FirstName";
            this.DriverFirstName.HeaderText = "First Name";
            this.DriverFirstName.Name = "DriverFirstName";
            // 
            // DriverLastName
            // 
            this.DriverLastName.DataPropertyName = "LastName";
            this.DriverLastName.HeaderText = "Last Name";
            this.DriverLastName.Name = "DriverLastName";
            // 
            // TrainingName
            // 
            this.TrainingName.DataPropertyName = "TrainingName";
            this.TrainingName.HeaderText = "Training Name";
            this.TrainingName.Name = "TrainingName";
            // 
            // TrainingSessionName
            // 
            this.TrainingSessionName.DataPropertyName = "TrainingSessionName";
            this.TrainingSessionName.HeaderText = "Training Session Name";
            this.TrainingSessionName.Name = "TrainingSessionName";
            // 
            // TrainingExpiryDate
            // 
            this.TrainingExpiryDate.DataPropertyName = "TrainingExpiryDate";
            this.TrainingExpiryDate.HeaderText = "Training Expiry Date";
            this.TrainingExpiryDate.Name = "TrainingExpiryDate";
            // 
            // FrmDriverExpiryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DgvTrainingDetails);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DgvQualificationDetails);
            this.Name = "FrmDriverExpiryView";
            this.Text = "Driver Qualification/Training Expiring";
            ((System.ComponentModel.ISupportInitialize)(this.DgvQualificationDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTrainingDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvQualificationDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualificationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualificationType;
        private System.Windows.Forms.DataGridViewTextBoxColumn LicenseNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn GeographicalTests;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualificationExpiryDate;
        private System.Windows.Forms.DataGridView DgvTrainingDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn TId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DriverFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DriverLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingSessionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingExpiryDate;
        private System.Windows.Forms.Label label2;
    }
}
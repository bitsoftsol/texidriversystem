﻿namespace TexiDriverSystem
{
    partial class FrmAppSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.BtnDriverApp = new System.Windows.Forms.Button();
            this.BtnAdminApp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(56, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(671, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Welcome to Texi Driver System, Please Click on appropriate Button to Continue";
            // 
            // BtnDriverApp
            // 
            this.BtnDriverApp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDriverApp.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDriverApp.FlatAppearance.BorderSize = 0;
            this.BtnDriverApp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnDriverApp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDriverApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDriverApp.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnDriverApp.Location = new System.Drawing.Point(191, 176);
            this.BtnDriverApp.Name = "BtnDriverApp";
            this.BtnDriverApp.Size = new System.Drawing.Size(195, 77);
            this.BtnDriverApp.TabIndex = 3;
            this.BtnDriverApp.Text = "Driver App";
            this.BtnDriverApp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnDriverApp.UseVisualStyleBackColor = false;
            this.BtnDriverApp.Click += new System.EventHandler(this.BtnDriverApp_Click);
            // 
            // BtnAdminApp
            // 
            this.BtnAdminApp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnAdminApp.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnAdminApp.FlatAppearance.BorderSize = 0;
            this.BtnAdminApp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnAdminApp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAdminApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdminApp.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnAdminApp.Location = new System.Drawing.Point(433, 176);
            this.BtnAdminApp.Name = "BtnAdminApp";
            this.BtnAdminApp.Size = new System.Drawing.Size(195, 77);
            this.BtnAdminApp.TabIndex = 3;
            this.BtnAdminApp.Text = "Admin App";
            this.BtnAdminApp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnAdminApp.UseVisualStyleBackColor = false;
            this.BtnAdminApp.Click += new System.EventHandler(this.BtnAdminApp_Click);
            // 
            // FrmAppSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnAdminApp);
            this.Controls.Add(this.BtnDriverApp);
            this.Controls.Add(this.label1);
            this.Name = "FrmAppSelection";
            this.Text = "Texi Driver System";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnDriverApp;
        private System.Windows.Forms.Button BtnAdminApp;
    }
}
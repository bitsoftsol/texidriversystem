﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StopWatch2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            stop.Enabled = false;
        }

        int milisec = 0;
        int sec = 0;
        int min = 0;
        int hour = 0;


        private void timer1_Tick(object sender, EventArgs e)
        {
            milisec++;
            if(milisec == 100)
            {
                sec++;
                milisec = 0;
            }
            if(sec == 60) { min++;
                sec = 0;
            }
            if(min == 60)
            {
                hour++;
                min = 0;
            }

            time.Text = hour.ToString() + " : " + min.ToString() + " : " + sec.ToString() + " : " + milisec.ToString();
        }

        private void Start_Click(object sender, EventArgs e)
        {
            Start.BackColor = Color.Red;
            stop.Enabled = true;
            stop.BackColor = Color.DodgerBlue;
            timer1.Start();
        }

        private void stop_Click(object sender, EventArgs e)
        {
            stop.BackColor = Color.Red;
            Start.BackColor = Color.DodgerBlue;
            timer1.Stop();
        }

        private void reset_Click(object sender, EventArgs e)
        {
            milisec = 0;
            hour = 0;
            sec = 0;
            min = 0;

            time.Text = hour.ToString() + " : " + min.ToString() + " : " + sec.ToString() + " : " + milisec.ToString();
        }
    }
}

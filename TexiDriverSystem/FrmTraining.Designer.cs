﻿namespace TexiDriverSystem
{
    partial class FrmTraining
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmbTrainings = new System.Windows.Forms.ComboBox();
            this.CmbDrivers = new System.Windows.Forms.ComboBox();
            this.TxtSession = new System.Windows.Forms.TextBox();
            this.DtpSessionStart = new System.Windows.Forms.DateTimePicker();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LblError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CmbTrainings
            // 
            this.CmbTrainings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.CmbTrainings.FormattingEnabled = true;
            this.CmbTrainings.Location = new System.Drawing.Point(51, 122);
            this.CmbTrainings.Name = "CmbTrainings";
            this.CmbTrainings.Size = new System.Drawing.Size(358, 25);
            this.CmbTrainings.TabIndex = 0;
            // 
            // CmbDrivers
            // 
            this.CmbDrivers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.CmbDrivers.FormattingEnabled = true;
            this.CmbDrivers.Location = new System.Drawing.Point(51, 60);
            this.CmbDrivers.Name = "CmbDrivers";
            this.CmbDrivers.Size = new System.Drawing.Size(358, 25);
            this.CmbDrivers.TabIndex = 1;
            // 
            // TxtSession
            // 
            this.TxtSession.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.TxtSession.Location = new System.Drawing.Point(51, 183);
            this.TxtSession.Name = "TxtSession";
            this.TxtSession.Size = new System.Drawing.Size(358, 23);
            this.TxtSession.TabIndex = 2;
            // 
            // DtpSessionStart
            // 
            this.DtpSessionStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.DtpSessionStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpSessionStart.Location = new System.Drawing.Point(51, 264);
            this.DtpSessionStart.Name = "DtpSessionStart";
            this.DtpSessionStart.Size = new System.Drawing.Size(358, 23);
            this.DtpSessionStart.TabIndex = 3;
            // 
            // BtnSave
            // 
            this.BtnSave.Font = new System.Drawing.Font("Arial", 14.45F);
            this.BtnSave.Location = new System.Drawing.Point(173, 319);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(122, 37);
            this.BtnSave.TabIndex = 4;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Select Driver";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "Select Training";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(47, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Enter Session Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(47, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "Select Start Date";
            // 
            // LblError
            // 
            this.LblError.AutoSize = true;
            this.LblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblError.ForeColor = System.Drawing.Color.Red;
            this.LblError.Location = new System.Drawing.Point(47, 206);
            this.LblError.Name = "LblError";
            this.LblError.Size = new System.Drawing.Size(0, 20);
            this.LblError.TabIndex = 5;
            // 
            // FrmTraining
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(486, 392);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LblError);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.DtpSessionStart);
            this.Controls.Add(this.TxtSession);
            this.Controls.Add(this.CmbDrivers);
            this.Controls.Add(this.CmbTrainings);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTraining";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shedule Training";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CmbTrainings;
        private System.Windows.Forms.ComboBox CmbDrivers;
        private System.Windows.Forms.TextBox TxtSession;
        private System.Windows.Forms.DateTimePicker DtpSessionStart;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LblError;
    }
}
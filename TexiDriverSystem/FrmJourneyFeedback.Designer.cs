﻿namespace TexiDriverSystem
{
    partial class FrmJourneyFeedback
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnEdit = new System.Windows.Forms.Button();
            this.TxtJourneyId = new System.Windows.Forms.TextBox();
            this.DgvJourney = new System.Windows.Forms.DataGridView();
            this.CmbIncident = new System.Windows.Forms.ComboBox();
            this.RtbRemarks = new System.Windows.Forms.RichTextBox();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DriverName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartOfDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartOfJourney = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndOfJourney = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgvJourney)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnEdit
            // 
            this.BtnEdit.Enabled = false;
            this.BtnEdit.Location = new System.Drawing.Point(130, 419);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(96, 34);
            this.BtnEdit.TabIndex = 9;
            this.BtnEdit.Text = "Edit";
            this.BtnEdit.UseVisualStyleBackColor = true;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // TxtJourneyId
            // 
            this.TxtJourneyId.Location = new System.Drawing.Point(30, 147);
            this.TxtJourneyId.Name = "TxtJourneyId";
            this.TxtJourneyId.ReadOnly = true;
            this.TxtJourneyId.Size = new System.Drawing.Size(306, 20);
            this.TxtJourneyId.TabIndex = 7;
            // 
            // DgvJourney
            // 
            this.DgvJourney.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvJourney.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvJourney.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvJourney.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.DriverName,
            this.StartOfDay,
            this.StartOfJourney,
            this.EndOfJourney});
            this.DgvJourney.Location = new System.Drawing.Point(372, 45);
            this.DgvJourney.Name = "DgvJourney";
            this.DgvJourney.Size = new System.Drawing.Size(530, 506);
            this.DgvJourney.TabIndex = 6;
            this.DgvJourney.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvJourney_CellClick);
            // 
            // CmbIncident
            // 
            this.CmbIncident.FormattingEnabled = true;
            this.CmbIncident.Items.AddRange(new object[] {
            "None",
            "Accidents",
            "Abusive Behaviour Towards Customers",
            "Speeding Offences",
            "Aggressive/Dangerous Driving",
            "Running a Red Light"});
            this.CmbIncident.Location = new System.Drawing.Point(30, 215);
            this.CmbIncident.Name = "CmbIncident";
            this.CmbIncident.Size = new System.Drawing.Size(306, 21);
            this.CmbIncident.TabIndex = 10;
            // 
            // RtbRemarks
            // 
            this.RtbRemarks.Location = new System.Drawing.Point(30, 291);
            this.RtbRemarks.Name = "RtbRemarks";
            this.RtbRemarks.Size = new System.Drawing.Size(306, 96);
            this.RtbRemarks.TabIndex = 11;
            this.RtbRemarks.Text = "";
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // DriverName
            // 
            this.DriverName.DataPropertyName = "DriverName";
            this.DriverName.HeaderText = "Driver Name";
            this.DriverName.Name = "DriverName";
            // 
            // StartOfDay
            // 
            this.StartOfDay.DataPropertyName = "StartOfDay";
            this.StartOfDay.HeaderText = "Day";
            this.StartOfDay.Name = "StartOfDay";
            // 
            // StartOfJourney
            // 
            this.StartOfJourney.DataPropertyName = "StartOfJourney";
            this.StartOfJourney.HeaderText = "Journey Start";
            this.StartOfJourney.Name = "StartOfJourney";
            // 
            // EndOfJourney
            // 
            this.EndOfJourney.DataPropertyName = "EndOfJourney";
            this.EndOfJourney.HeaderText = "Journey End";
            this.EndOfJourney.Name = "EndOfJourney";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "Journey Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 24);
            this.label2.TabIndex = 12;
            this.label2.Text = "Incident ?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 24);
            this.label3.TabIndex = 12;
            this.label3.Text = "Remarks";
            // 
            // FrmJourneyFeedback
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 603);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RtbRemarks);
            this.Controls.Add(this.CmbIncident);
            this.Controls.Add(this.BtnEdit);
            this.Controls.Add(this.TxtJourneyId);
            this.Controls.Add(this.DgvJourney);
            this.Name = "FrmJourneyFeedback";
            this.Text = "Journey Feedback";
            ((System.ComponentModel.ISupportInitialize)(this.DgvJourney)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.TextBox TxtJourneyId;
        private System.Windows.Forms.DataGridView DgvJourney;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn DriverName;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartOfDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartOfJourney;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndOfJourney;
        private System.Windows.Forms.ComboBox CmbIncident;
        private System.Windows.Forms.RichTextBox RtbRemarks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}
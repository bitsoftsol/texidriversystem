﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace TexiDriverSystem
{
    public partial class FrmDriver : Form
    {
        public FrmDriver()
        {
            InitializeComponent();
        }

        private void FrmDriver_Load(object sender, EventArgs e)
        {
            using (var con = new Taxi_Driver_SystemEntities())
            {
                var list = con.Qualifications.ToList();
                cmbQualifaction.DataSource = list;
                cmbQualifaction.ValueMember = "Id";
                cmbQualifaction.DisplayMember = "QualificationName";

                var trun = con.Trainings.ToList();
                cmbTrain.DataSource = trun;
                cmbTrain.ValueMember = "Id";
                cmbTrain.DisplayMember = "CourseName";
            }
        }

        private void cmbQualifaction_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            dgvQua.Rows.Add(cmbQualifaction.Text);
        }

        private void cmbTrain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            dgvTrain.Rows.Add(cmbTrain.Text);
        }

        private void btnSaveQua_Click(object sender, EventArgs e)
        {
            dgvQua.Rows.Add(cmbQualifaction.SelectedValue.ToString(), cmbQualifaction.Text, cmbQtype.Text,
                txtLicenseNum.Text, cmbGeotest.Text, quaExpire.Value);
        }

        private void btnAddTrain_Click(object sender, EventArgs e)
        {
            dgvTrain.Rows.Add(cmbTrain.SelectedValue, cmbTrain.Text, trainingExp.Value);
        }

        private void btnAddDriver_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFname.Text))
            {
                MessageBox.Show(@"FirstName Required");
                return;
            }

            if (string.IsNullOrEmpty(txtLastN.Text))
            {
                MessageBox.Show(@"LastName Required");
                return;
            }

            using (var db = new Taxi_Driver_SystemEntities())
            {
                var getId = db.Drivers.ToList();

                var go = new Drivers
                {
                    Id = getId.Count + 1,
                    FirstName = txtFname.Text,
                    LastName = txtLastN.Text,
                    Dob = DateTime.Parse(txtDob.Text),
                    Mobile = txtPhoneNum.Text
                };
                var driverId = db.Drivers.Add(go).Id;

                var DriverQId = db.DriverQualifications.ToList();
                for (var i = 0; i < dgvQua.RowCount - 1; i++)
                {
                    var dgvQ = dgvQua.Rows[i];

                    var qId = int.Parse(dgvQ.Cells[0].Value.ToString());
                    var qua = new DriverQualifications
                    {
                        Id = DriverQId.Count + 1,
                        DriverId = driverId,
                        LicenseNumber = dgvQ.Cells[3].Value.ToString(),
                        GeographicalTests = dgvQ.Cells[4].Value.ToString(),
                        QualificationExpiryDate = DateTime.Parse(dgvQ.Cells[5].Value.ToString()),
                        QualificationId = qId,
                        QualificationType = cmbQtype.SelectedIndex
                    };
                    db.DriverQualifications.Add(qua);
                }

                var DriverTId = db.DriverTrainings.ToList();
                for (var i = 0; i < dgvTrain.RowCount - 1; i++)
                {
                    var dgvT = dgvTrain.Rows[i];
                    var tId = dgvT.Cells[0].Value.ToString();
                    var train = new DriverTrainings
                    {
                        Id = DriverTId.Count + 1,
                        DriverId = driverId,
                        TrainingId = Convert.ToInt64(tId),
                        TrainingExpiryDate = DateTime.Parse(dgvT.Cells[2].Value.ToString())
                    };
                    db.DriverTrainings.Add(train);
                }

                if (db.SaveChanges() > 0)
                {
                    MessageBox.Show(@"Driver Added Successfully");
                }
            }
        }

        private void dgvTrain_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dgv = dgvTrain.SelectedRows[0].Cells;
            cmbTrain.Text = dgv[1].Value.ToString();
            trainingExp.Value = DateTime.Parse(dgv[2].Value.ToString());
            btnUpdate2.Enabled = true;
        }

        private void dgvQua_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dgv = dgvQua.SelectedRows[0].Cells;
            cmbQualifaction.Text = dgv[1].Value.ToString();
            cmbQtype.Text = dgv[2].Value.ToString();
            txtLicenseNum.Text = dgv[3].Value.ToString();
            cmbGeotest.Text = dgv[4].Value.ToString();
            quaExpire.Value = DateTime.Parse(dgv[5].Value.ToString());
            btnUpdate.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var dgv = dgvQua.SelectedRows[0].Cells;
            dgv[1].Value = cmbQualifaction.Text;
            dgv[2].Value = cmbQtype.Text;
            dgv[3].Value = txtLicenseNum.Text;
            dgv[4].Value = cmbGeotest.Text;
            quaExpire.Value = DateTime.Parse(dgv[5].Value.ToString());
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtFname.Text = string.Empty;
            txtLastN.Text = string.Empty;
            txtDob.Text = string.Empty;
            txtPhoneNum.Text = string.Empty;
            txtLicenseNum.Text = string.Empty;
            cmbQualifaction.Text = string.Empty;
            cmbQtype.Text = string.Empty;
            cmbGeotest.Text = string.Empty;
            cmbTrain.Text = string.Empty;
        }

        private void dgvTrain_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dgvTrain.Rows.RemoveAt(e.RowIndex);
        }

        private void dgvQua_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dgvQua.Rows.RemoveAt(e.RowIndex);
        }
    }
}
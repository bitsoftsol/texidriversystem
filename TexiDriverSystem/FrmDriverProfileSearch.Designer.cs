﻿namespace TexiDriverSystem
{
    partial class FrmDriverProfileSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtSerach = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DgvDrivers = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dob = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LblDriverId = new System.Windows.Forms.Label();
            this.BtnShowProfile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DgvDrivers)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtSerach
            // 
            this.TxtSerach.Location = new System.Drawing.Point(28, 30);
            this.TxtSerach.Name = "TxtSerach";
            this.TxtSerach.Size = new System.Drawing.Size(688, 20);
            this.TxtSerach.TabIndex = 0;
            this.TxtSerach.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtSerach_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter Driver Name For Search";
            // 
            // DgvDrivers
            // 
            this.DgvDrivers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvDrivers.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvDrivers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvDrivers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.FirstName,
            this.LastName,
            this.Dob,
            this.Mobile});
            this.DgvDrivers.Location = new System.Drawing.Point(29, 56);
            this.DgvDrivers.Name = "DgvDrivers";
            this.DgvDrivers.Size = new System.Drawing.Size(813, 482);
            this.DgvDrivers.TabIndex = 2;
            this.DgvDrivers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvDrivers_CellClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "First Name";
            this.FirstName.Name = "FirstName";
            // 
            // LastName
            // 
            this.LastName.DataPropertyName = "LastName";
            this.LastName.HeaderText = "Last Name";
            this.LastName.Name = "LastName";
            // 
            // Dob
            // 
            this.Dob.DataPropertyName = "Dob";
            this.Dob.HeaderText = "Date Of Birth";
            this.Dob.Name = "Dob";
            // 
            // Mobile
            // 
            this.Mobile.DataPropertyName = "Mobile";
            this.Mobile.HeaderText = "Mobile Number";
            this.Mobile.Name = "Mobile";
            // 
            // LblDriverId
            // 
            this.LblDriverId.AutoSize = true;
            this.LblDriverId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverId.ForeColor = System.Drawing.Color.Red;
            this.LblDriverId.Location = new System.Drawing.Point(253, 9);
            this.LblDriverId.Name = "LblDriverId";
            this.LblDriverId.Size = new System.Drawing.Size(0, 20);
            this.LblDriverId.TabIndex = 1;
            // 
            // BtnShowProfile
            // 
            this.BtnShowProfile.Enabled = false;
            this.BtnShowProfile.Location = new System.Drawing.Point(732, 28);
            this.BtnShowProfile.Name = "BtnShowProfile";
            this.BtnShowProfile.Size = new System.Drawing.Size(90, 23);
            this.BtnShowProfile.TabIndex = 3;
            this.BtnShowProfile.Text = "Show Profile";
            this.BtnShowProfile.UseVisualStyleBackColor = true;
            this.BtnShowProfile.Click += new System.EventHandler(this.BtnShowProfile_Click);
            // 
            // FrmDriverProfileSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 550);
            this.Controls.Add(this.BtnShowProfile);
            this.Controls.Add(this.DgvDrivers);
            this.Controls.Add(this.LblDriverId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtSerach);
            this.Name = "FrmDriverProfileSearch";
            this.Text = "Driver Profile Search";
            ((System.ComponentModel.ISupportInitialize)(this.DgvDrivers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtSerach;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DgvDrivers;
        private System.Windows.Forms.Label LblDriverId;
        private System.Windows.Forms.Button BtnShowProfile;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dob;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mobile;
    }
}
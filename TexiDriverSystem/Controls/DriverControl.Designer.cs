﻿namespace TexiDriverSystem.Controls
{
    partial class DriverControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnDriverExpiry = new System.Windows.Forms.Button();
            this.BtnDriverSearch = new System.Windows.Forms.Button();
            this.BtnDrivers = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnDriverExpiry
            // 
            this.BtnDriverExpiry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDriverExpiry.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDriverExpiry.FlatAppearance.BorderSize = 0;
            this.BtnDriverExpiry.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnDriverExpiry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDriverExpiry.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDriverExpiry.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnDriverExpiry.Location = new System.Drawing.Point(441, 43);
            this.BtnDriverExpiry.Name = "BtnDriverExpiry";
            this.BtnDriverExpiry.Size = new System.Drawing.Size(195, 137);
            this.BtnDriverExpiry.TabIndex = 2;
            this.BtnDriverExpiry.Text = "Driver With Training/Qualification Expiring in Next 30 Days";
            this.BtnDriverExpiry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnDriverExpiry.UseVisualStyleBackColor = false;
            this.BtnDriverExpiry.Click += new System.EventHandler(this.BtnDriverExpiry_Click);
            // 
            // BtnDriverSearch
            // 
            this.BtnDriverSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDriverSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDriverSearch.FlatAppearance.BorderSize = 0;
            this.BtnDriverSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnDriverSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDriverSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDriverSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnDriverSearch.Location = new System.Drawing.Point(240, 43);
            this.BtnDriverSearch.Name = "BtnDriverSearch";
            this.BtnDriverSearch.Size = new System.Drawing.Size(195, 137);
            this.BtnDriverSearch.TabIndex = 3;
            this.BtnDriverSearch.Text = "Search Driver";
            this.BtnDriverSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnDriverSearch.UseVisualStyleBackColor = false;
            this.BtnDriverSearch.Click += new System.EventHandler(this.BtnDriverSearch_Click);
            // 
            // BtnDrivers
            // 
            this.BtnDrivers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDrivers.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnDrivers.FlatAppearance.BorderSize = 0;
            this.BtnDrivers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnDrivers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDrivers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDrivers.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnDrivers.Location = new System.Drawing.Point(39, 43);
            this.BtnDrivers.Name = "BtnDrivers";
            this.BtnDrivers.Size = new System.Drawing.Size(195, 137);
            this.BtnDrivers.TabIndex = 4;
            this.BtnDrivers.Text = "Add/Edit Delete Drivers";
            this.BtnDrivers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnDrivers.UseVisualStyleBackColor = false;
            this.BtnDrivers.Click += new System.EventHandler(this.BtnDrivers_Click);
            // 
            // DriverControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BtnDriverExpiry);
            this.Controls.Add(this.BtnDriverSearch);
            this.Controls.Add(this.BtnDrivers);
            this.Name = "DriverControl";
            this.Size = new System.Drawing.Size(686, 598);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnDriverExpiry;
        private System.Windows.Forms.Button BtnDriverSearch;
        private System.Windows.Forms.Button BtnDrivers;
    }
}

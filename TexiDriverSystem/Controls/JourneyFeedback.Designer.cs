﻿namespace TexiDriverSystem.Controls
{
    partial class JourneyFeedback
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnRecordJourneyFeed = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnRecordJourneyFeed
            // 
            this.BtnRecordJourneyFeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnRecordJourneyFeed.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnRecordJourneyFeed.FlatAppearance.BorderSize = 0;
            this.BtnRecordJourneyFeed.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnRecordJourneyFeed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnRecordJourneyFeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecordJourneyFeed.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnRecordJourneyFeed.Location = new System.Drawing.Point(25, 34);
            this.BtnRecordJourneyFeed.Name = "BtnRecordJourneyFeed";
            this.BtnRecordJourneyFeed.Size = new System.Drawing.Size(195, 137);
            this.BtnRecordJourneyFeed.TabIndex = 5;
            this.BtnRecordJourneyFeed.Text = "Record Journey Feedback";
            this.BtnRecordJourneyFeed.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnRecordJourneyFeed.UseVisualStyleBackColor = false;
            this.BtnRecordJourneyFeed.Click += new System.EventHandler(this.BtnRecordJourneyFeed_Click);
            // 
            // JourneyFeedback
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BtnRecordJourneyFeed);
            this.Name = "JourneyFeedback";
            this.Size = new System.Drawing.Size(686, 598);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnRecordJourneyFeed;
    }
}

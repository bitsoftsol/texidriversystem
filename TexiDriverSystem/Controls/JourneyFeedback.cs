﻿using System.Windows.Forms;

namespace TexiDriverSystem.Controls
{
    public partial class JourneyFeedback : UserControl
    {
        #region
        private static JourneyFeedback _instance { get; set; }
        private JourneyFeedback()
        {
            InitializeComponent();
        }
        public static JourneyFeedback Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new JourneyFeedback();
                }
                return _instance;
            }
        }
        #endregion

        private void BtnRecordJourneyFeed_Click(object sender, System.EventArgs e)
        {
            FrmJourneyFeedback frmJourneyFeedback = new FrmJourneyFeedback();
            frmJourneyFeedback.Show();
        }
    }
}

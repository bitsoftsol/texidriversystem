﻿namespace TexiDriverSystem.Controls
{
    partial class TrainingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTrainings = new System.Windows.Forms.Button();
            this.BtnSheduleSession = new System.Windows.Forms.Button();
            this.BtnSessionOutcome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTrainings
            // 
            this.btnTrainings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.btnTrainings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.btnTrainings.FlatAppearance.BorderSize = 0;
            this.btnTrainings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTrainings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrainings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrainings.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnTrainings.Location = new System.Drawing.Point(44, 42);
            this.btnTrainings.Name = "btnTrainings";
            this.btnTrainings.Size = new System.Drawing.Size(195, 137);
            this.btnTrainings.TabIndex = 1;
            this.btnTrainings.Text = "Add/Edit Delete Training";
            this.btnTrainings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTrainings.UseVisualStyleBackColor = false;
            this.btnTrainings.Click += new System.EventHandler(this.btnTrainings_Click);
            // 
            // BtnSheduleSession
            // 
            this.BtnSheduleSession.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnSheduleSession.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnSheduleSession.FlatAppearance.BorderSize = 0;
            this.BtnSheduleSession.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnSheduleSession.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSheduleSession.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSheduleSession.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnSheduleSession.Location = new System.Drawing.Point(245, 42);
            this.BtnSheduleSession.Name = "BtnSheduleSession";
            this.BtnSheduleSession.Size = new System.Drawing.Size(195, 137);
            this.BtnSheduleSession.TabIndex = 1;
            this.BtnSheduleSession.Text = "Shedule Training Sessions";
            this.BtnSheduleSession.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnSheduleSession.UseVisualStyleBackColor = false;
            this.BtnSheduleSession.Click += new System.EventHandler(this.BtnSheduleSession_Click);
            // 
            // BtnSessionOutcome
            // 
            this.BtnSessionOutcome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnSessionOutcome.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnSessionOutcome.FlatAppearance.BorderSize = 0;
            this.BtnSessionOutcome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnSessionOutcome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSessionOutcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSessionOutcome.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnSessionOutcome.Location = new System.Drawing.Point(446, 42);
            this.BtnSessionOutcome.Name = "BtnSessionOutcome";
            this.BtnSessionOutcome.Size = new System.Drawing.Size(195, 137);
            this.BtnSessionOutcome.TabIndex = 1;
            this.BtnSessionOutcome.Text = "Record Session Outcome";
            this.BtnSessionOutcome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnSessionOutcome.UseVisualStyleBackColor = false;
            this.BtnSessionOutcome.Click += new System.EventHandler(this.BtnSessionOutcome_Click);
            // 
            // TrainingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BtnSessionOutcome);
            this.Controls.Add(this.BtnSheduleSession);
            this.Controls.Add(this.btnTrainings);
            this.Name = "TrainingControl";
            this.Size = new System.Drawing.Size(686, 598);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTrainings;
        private System.Windows.Forms.Button BtnSheduleSession;
        private System.Windows.Forms.Button BtnSessionOutcome;
    }
}

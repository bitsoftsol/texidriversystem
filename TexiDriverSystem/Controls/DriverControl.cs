﻿using System;
using System.Windows.Forms;

namespace TexiDriverSystem.Controls
{
    public partial class DriverControl : UserControl
    {
        #region
        private static DriverControl _instance { get; set; }
        private DriverControl()
        {
            InitializeComponent();
        }
        public static DriverControl Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DriverControl();
                }
                return _instance;
            }
        }
        #endregion

        private void BtnDrivers_Click(object sender, EventArgs e)
        {
            FrmDriver frmDriver = new FrmDriver();
            frmDriver.Show();
        }

        private void BtnDriverSearch_Click(object sender, EventArgs e)
        {
            FrmDriverProfileSearch frmDriverProfileSearch = new FrmDriverProfileSearch();
            frmDriverProfileSearch.Show();
        }

        private void BtnDriverExpiry_Click(object sender, EventArgs e)
        {
            FrmDriverExpiryView frmDriverExpiryView = new FrmDriverExpiryView();
            frmDriverExpiryView.Show();
        }
    }
}

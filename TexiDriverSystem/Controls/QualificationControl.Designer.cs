﻿namespace TexiDriverSystem.Controls
{
    partial class QualificationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnQualifications = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnQualifications
            // 
            this.BtnQualifications.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnQualifications.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnQualifications.FlatAppearance.BorderSize = 0;
            this.BtnQualifications.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnQualifications.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnQualifications.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQualifications.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnQualifications.Location = new System.Drawing.Point(40, 38);
            this.BtnQualifications.Name = "BtnQualifications";
            this.BtnQualifications.Size = new System.Drawing.Size(195, 137);
            this.BtnQualifications.TabIndex = 2;
            this.BtnQualifications.Text = "Add/Edit Delete Qualification";
            this.BtnQualifications.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnQualifications.UseVisualStyleBackColor = false;
            this.BtnQualifications.Click += new System.EventHandler(this.BtnQualifications_Click);
            // 
            // QualificationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BtnQualifications);
            this.Name = "QualificationControl";
            this.Size = new System.Drawing.Size(686, 598);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnQualifications;
    }
}

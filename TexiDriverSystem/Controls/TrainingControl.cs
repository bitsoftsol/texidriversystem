﻿using System;
using System.Windows.Forms;

namespace TexiDriverSystem.Controls
{
    public partial class TrainingControl : UserControl
    {
        #region
        private static TrainingControl _instance { get; set; }
        private TrainingControl()
        {
            InitializeComponent();
        }
        public static TrainingControl Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TrainingControl();
                }
                return _instance;
            }
        }
        #endregion

        private void btnTrainings_Click(object sender, EventArgs e)
        {
            FrmDriverTrainings frmDriverTrainings = new FrmDriverTrainings();
            frmDriverTrainings.Show();
        }

        private void BtnSheduleSession_Click(object sender, EventArgs e)
        {
            FrmTraining frmTraining = new FrmTraining();
            frmTraining.Show();
        }

        private void BtnSessionOutcome_Click(object sender, EventArgs e)
        {
            FrmTrainingOutCome frmTrainingOutCome = new FrmTrainingOutCome();
            frmTrainingOutCome.Show();
        }
    }
}

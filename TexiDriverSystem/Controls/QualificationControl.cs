﻿using System.Windows.Forms;

namespace TexiDriverSystem.Controls
{
    public partial class QualificationControl : UserControl
    {
        #region
        private static QualificationControl _instance { get; set; }
        private QualificationControl()
        {
            InitializeComponent();
        }
        public static QualificationControl Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new QualificationControl();
                }
                return _instance;
            }
        }
        #endregion

        private void BtnQualifications_Click(object sender, System.EventArgs e)
        {
            FrmAddQualification frmAddQualification = new FrmAddQualification();
            frmAddQualification.Show();
        }
    }
}

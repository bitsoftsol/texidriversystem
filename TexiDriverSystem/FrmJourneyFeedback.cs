﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using TexiDriverSystem.View_Models;

namespace TexiDriverSystem
{
    public partial class FrmJourneyFeedback : Form
    {
        public FrmJourneyFeedback()
        {
            InitializeComponent();
            OnLoad();
        }
        private void OnLoad()
        {
            using (var db = new Taxi_Driver_SystemEntities())
            {
                var journeys = db.DriverJourneyLogs.Where(r => r.Status == 0).ToList();
                var journeyVm = new List<JourneyFeedbackVm>();
                for (int i = 0; i < journeys.Count; i++)
                {
                    var journeyVmInsert = new JourneyFeedbackVm();
                    journeyVmInsert.Id = journeys[i].Id;
                    journeyVmInsert.StartOfDay = journeys[i].DriverDayLogs.StartOfDay;
                    journeyVmInsert.DriverName = journeys[i].DriverDayLogs.Drivers.FirstName + " " + journeys[i].DriverDayLogs.Drivers.LastName;
                    journeyVmInsert.StartOfJourney = journeys[i].StartOfJourney;
                    journeyVmInsert.EndOfJourney = journeys[i].EndOfJourney;
                    TimeSpan diff = journeyVmInsert.EndOfJourney - journeyVmInsert.StartOfJourney;
                    int hours = diff.Hours;
                    int min = diff.Minutes;
                    int seconds = diff.Seconds;
                    journeyVmInsert.Duration = hours + " : " + min + " : " + seconds;
                    journeyVm.Insert(i, journeyVmInsert);
                }
                DgvJourney.AutoGenerateColumns = false;
                DgvJourney.DataSource = journeyVm;
            }
        }

        private void DgvJourney_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null)
            {
                return;
            }
            if (dgv.CurrentRow.Selected)
            {
                TxtJourneyId.Text = dgv.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                BtnEdit.Enabled = true;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            long id = Convert.ToInt64(TxtJourneyId.Text); ;
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                var journey = db.DriverJourneyLogs.SingleOrDefault(r => r.Id == id);
                if (journey != null)
                {
                    journey.Status = (int)CmbIncident.SelectedIndex;
                    db.SaveChanges();
                }
                var journeyRemark = new JourneyFeedbacks();
                var newId = db.JourneyFeedbacks.OrderByDescending(r => r.Id).FirstOrDefault();
                journeyRemark.Id = newId == null ? 1 : newId.Id + 1;
                journeyRemark.JournyId = journey.Id;
                journeyRemark.Remarks = RtbRemarks.Text;
                db.JourneyFeedbacks.Add(journeyRemark);
                db.SaveChanges();
            }
            MessageBox.Show("Data Updated Successfully");
        }
    }
}

﻿namespace TexiDriverSystem
{
    partial class FrmDriver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFname = new System.Windows.Forms.TextBox();
            this.txtLastN = new System.Windows.Forms.TextBox();
            this.txtDob = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhoneNum = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbQualifaction = new System.Windows.Forms.ComboBox();
            this.dgvQua = new System.Windows.Forms.DataGridView();
            this.qId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.licence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.geo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expire = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSaveQua = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbGeotest = new System.Windows.Forms.ComboBox();
            this.txtLicenseNum = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.quaExpire = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbQtype = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAddTrain = new System.Windows.Forms.Button();
            this.trainingExp = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.dgvTrain = new System.Windows.Forms.DataGridView();
            this.tId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expire2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbTrain = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnAddDriver = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnUpdate2 = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQua)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrain)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label1.Location = new System.Drawing.Point(18, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "FirstName:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label2.Location = new System.Drawing.Point(209, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "LastName:";
            // 
            // txtFname
            // 
            this.txtFname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.txtFname.Location = new System.Drawing.Point(21, 49);
            this.txtFname.Name = "txtFname";
            this.txtFname.Size = new System.Drawing.Size(175, 23);
            this.txtFname.TabIndex = 0;
            // 
            // txtLastN
            // 
            this.txtLastN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.txtLastN.Location = new System.Drawing.Point(212, 49);
            this.txtLastN.Name = "txtLastN";
            this.txtLastN.Size = new System.Drawing.Size(175, 23);
            this.txtLastN.TabIndex = 1;
            // 
            // txtDob
            // 
            this.txtDob.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.55F);
            this.txtDob.Location = new System.Drawing.Point(404, 49);
            this.txtDob.Mask = "00/00/0000";
            this.txtDob.Name = "txtDob";
            this.txtDob.Size = new System.Drawing.Size(175, 23);
            this.txtDob.TabIndex = 2;
            this.txtDob.ValidatingType = typeof(System.DateTime);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label3.Location = new System.Drawing.Point(401, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "DOB:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label4.Location = new System.Drawing.Point(601, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "PhoneNumber:";
            // 
            // txtPhoneNum
            // 
            this.txtPhoneNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.55F);
            this.txtPhoneNum.Location = new System.Drawing.Point(604, 49);
            this.txtPhoneNum.Mask = "(999) 000-00000";
            this.txtPhoneNum.Name = "txtPhoneNum";
            this.txtPhoneNum.Size = new System.Drawing.Size(175, 23);
            this.txtPhoneNum.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label5.Location = new System.Drawing.Point(18, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Qualification:";
            // 
            // cmbQualifaction
            // 
            this.cmbQualifaction.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.cmbQualifaction.FormattingEnabled = true;
            this.cmbQualifaction.Location = new System.Drawing.Point(21, 51);
            this.cmbQualifaction.Name = "cmbQualifaction";
            this.cmbQualifaction.Size = new System.Drawing.Size(175, 25);
            this.cmbQualifaction.TabIndex = 0;
            this.cmbQualifaction.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbQualifaction_KeyDown);
            // 
            // dgvQua
            // 
            this.dgvQua.AllowUserToAddRows = false;
            this.dgvQua.AllowUserToDeleteRows = false;
            this.dgvQua.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvQua.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQua.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.qId,
            this.Qua,
            this.type,
            this.licence,
            this.geo,
            this.expire});
            this.dgvQua.Location = new System.Drawing.Point(206, 32);
            this.dgvQua.Name = "dgvQua";
            this.dgvQua.ReadOnly = true;
            this.dgvQua.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvQua.Size = new System.Drawing.Size(406, 341);
            this.dgvQua.TabIndex = 13;
            this.dgvQua.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvQua_CellMouseClick);
            this.dgvQua.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvQua_CellMouseDoubleClick);
            // 
            // qId
            // 
            this.qId.HeaderText = "qId";
            this.qId.Name = "qId";
            this.qId.ReadOnly = true;
            this.qId.Visible = false;
            // 
            // Qua
            // 
            this.Qua.HeaderText = "Qualifications";
            this.Qua.Name = "Qua";
            this.Qua.ReadOnly = true;
            // 
            // type
            // 
            this.type.HeaderText = "Type";
            this.type.Name = "type";
            this.type.ReadOnly = true;
            // 
            // licence
            // 
            this.licence.HeaderText = "LicenceNum";
            this.licence.Name = "licence";
            this.licence.ReadOnly = true;
            // 
            // geo
            // 
            this.geo.HeaderText = "GeoTests";
            this.geo.Name = "geo";
            this.geo.ReadOnly = true;
            // 
            // expire
            // 
            this.expire.HeaderText = "Expire";
            this.expire.Name = "expire";
            this.expire.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFname);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtDob);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtLastN);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtPhoneNum);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(793, 94);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic Information";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnUpdate);
            this.groupBox2.Controls.Add(this.btnSaveQua);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cmbGeotest);
            this.groupBox2.Controls.Add(this.txtLicenseNum);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.quaExpire);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cmbQtype);
            this.groupBox2.Controls.Add(this.dgvQua);
            this.groupBox2.Controls.Add(this.cmbQualifaction);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(622, 380);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Qualications";
            // 
            // btnSaveQua
            // 
            this.btnSaveQua.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveQua.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnSaveQua.Location = new System.Drawing.Point(21, 343);
            this.btnSaveQua.Name = "btnSaveQua";
            this.btnSaveQua.Size = new System.Drawing.Size(73, 30);
            this.btnSaveQua.TabIndex = 5;
            this.btnSaveQua.Text = "Add";
            this.btnSaveQua.UseVisualStyleBackColor = true;
            this.btnSaveQua.Click += new System.EventHandler(this.btnSaveQua_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label11.Location = new System.Drawing.Point(18, 226);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "GeographicalTests:";
            // 
            // cmbGeotest
            // 
            this.cmbGeotest.AutoCompleteCustomSource.AddRange(new string[] {
            "Central London",
            "North London",
            "South London",
            "East London",
            "West London"});
            this.cmbGeotest.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbGeotest.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbGeotest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.cmbGeotest.FormattingEnabled = true;
            this.cmbGeotest.Items.AddRange(new object[] {
            "Central London",
            "North London",
            "South London",
            "East London",
            "West London"});
            this.cmbGeotest.Location = new System.Drawing.Point(21, 246);
            this.cmbGeotest.Name = "cmbGeotest";
            this.cmbGeotest.Size = new System.Drawing.Size(175, 25);
            this.cmbGeotest.TabIndex = 3;
            // 
            // txtLicenseNum
            // 
            this.txtLicenseNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.txtLicenseNum.Location = new System.Drawing.Point(21, 178);
            this.txtLicenseNum.Name = "txtLicenseNum";
            this.txtLicenseNum.Size = new System.Drawing.Size(175, 23);
            this.txtLicenseNum.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label8.Location = new System.Drawing.Point(18, 158);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 17);
            this.label8.TabIndex = 25;
            this.label8.Text = "LicenseNumber:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label10.Location = new System.Drawing.Point(18, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 17);
            this.label10.TabIndex = 15;
            this.label10.Text = "Qualification Type:";
            // 
            // quaExpire
            // 
            this.quaExpire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.quaExpire.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.quaExpire.Location = new System.Drawing.Point(21, 307);
            this.quaExpire.Name = "quaExpire";
            this.quaExpire.Size = new System.Drawing.Size(175, 23);
            this.quaExpire.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label7.Location = new System.Drawing.Point(18, 287);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "Qualification Expire:";
            // 
            // cmbQtype
            // 
            this.cmbQtype.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.cmbQtype.FormattingEnabled = true;
            this.cmbQtype.Items.AddRange(new object[] {
            "Driving License",
            "Geographical Tests"});
            this.cmbQtype.Location = new System.Drawing.Point(21, 113);
            this.cmbQtype.Name = "cmbQtype";
            this.cmbQtype.Size = new System.Drawing.Size(175, 25);
            this.cmbQtype.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnUpdate2);
            this.groupBox3.Controls.Add(this.btnAddTrain);
            this.groupBox3.Controls.Add(this.trainingExp);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.dgvTrain);
            this.groupBox3.Controls.Add(this.cmbTrain);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Location = new System.Drawing.Point(640, 118);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(553, 380);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Training";
            // 
            // btnAddTrain
            // 
            this.btnAddTrain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddTrain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnAddTrain.Location = new System.Drawing.Point(341, 46);
            this.btnAddTrain.Name = "btnAddTrain";
            this.btnAddTrain.Size = new System.Drawing.Size(64, 30);
            this.btnAddTrain.TabIndex = 2;
            this.btnAddTrain.Text = "Add";
            this.btnAddTrain.UseVisualStyleBackColor = true;
            this.btnAddTrain.Click += new System.EventHandler(this.btnAddTrain_Click);
            // 
            // trainingExp
            // 
            this.trainingExp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.trainingExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.trainingExp.Location = new System.Drawing.Point(202, 51);
            this.trainingExp.Name = "trainingExp";
            this.trainingExp.Size = new System.Drawing.Size(133, 23);
            this.trainingExp.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label13.Location = new System.Drawing.Point(199, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 17);
            this.label13.TabIndex = 22;
            this.label13.Text = "Training Expire:";
            // 
            // dgvTrain
            // 
            this.dgvTrain.AllowUserToAddRows = false;
            this.dgvTrain.AllowUserToDeleteRows = false;
            this.dgvTrain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTrain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTrain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tId,
            this.dataGridViewTextBoxColumn1,
            this.expire2});
            this.dgvTrain.Location = new System.Drawing.Point(21, 93);
            this.dgvTrain.Name = "dgvTrain";
            this.dgvTrain.ReadOnly = true;
            this.dgvTrain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTrain.Size = new System.Drawing.Size(384, 280);
            this.dgvTrain.TabIndex = 13;
            this.dgvTrain.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvTrain_CellMouseClick);
            this.dgvTrain.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvTrain_CellMouseDoubleClick);
            // 
            // tId
            // 
            this.tId.HeaderText = "tId";
            this.tId.Name = "tId";
            this.tId.ReadOnly = true;
            this.tId.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Training";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // expire2
            // 
            this.expire2.HeaderText = "Expire";
            this.expire2.Name = "expire2";
            this.expire2.ReadOnly = true;
            // 
            // cmbTrain
            // 
            this.cmbTrain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.cmbTrain.FormattingEnabled = true;
            this.cmbTrain.Location = new System.Drawing.Point(21, 51);
            this.cmbTrain.Name = "cmbTrain";
            this.cmbTrain.Size = new System.Drawing.Size(175, 25);
            this.cmbTrain.TabIndex = 0;
            this.cmbTrain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTrain_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label14.Location = new System.Drawing.Point(18, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 17);
            this.label14.TabIndex = 10;
            this.label14.Text = "Training:";
            // 
            // btnAddDriver
            // 
            this.btnAddDriver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnAddDriver.Location = new System.Drawing.Point(1087, 505);
            this.btnAddDriver.Name = "btnAddDriver";
            this.btnAddDriver.Size = new System.Drawing.Size(109, 30);
            this.btnAddDriver.TabIndex = 0;
            this.btnAddDriver.Text = "Add Driver";
            this.btnAddDriver.UseVisualStyleBackColor = true;
            this.btnAddDriver.Click += new System.EventHandler(this.btnAddDriver_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnUpdate.Location = new System.Drawing.Point(123, 343);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(73, 30);
            this.btnUpdate.TabIndex = 29;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnUpdate2
            // 
            this.btnUpdate2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate2.Enabled = false;
            this.btnUpdate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnUpdate2.Location = new System.Drawing.Point(411, 46);
            this.btnUpdate2.Name = "btnUpdate2";
            this.btnUpdate2.Size = new System.Drawing.Size(73, 30);
            this.btnUpdate2.TabIndex = 23;
            this.btnUpdate2.Text = "Update";
            this.btnUpdate2.UseVisualStyleBackColor = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.55F);
            this.linkLabel1.Location = new System.Drawing.Point(999, 517);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(78, 18);
            this.linkLabel1.TabIndex = 30;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Clear form";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // FrmDriver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1208, 547);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.btnAddDriver);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDriver";
            this.Text = "Add Drivers";
            this.Load += new System.EventHandler(this.FrmDriver_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQua)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFname;
        private System.Windows.Forms.TextBox txtLastN;
        private System.Windows.Forms.MaskedTextBox txtDob;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtPhoneNum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbQualifaction;
        private System.Windows.Forms.DataGridView dgvQua;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker quaExpire;
        private System.Windows.Forms.Button btnSaveQua;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbQtype;
        private System.Windows.Forms.TextBox txtLicenseNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbGeotest;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnAddTrain;
        private System.Windows.Forms.DateTimePicker trainingExp;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dgvTrain;
        private System.Windows.Forms.ComboBox cmbTrain;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnAddDriver;
        private System.Windows.Forms.DataGridViewTextBoxColumn qId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qua;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn licence;
        private System.Windows.Forms.DataGridViewTextBoxColumn geo;
        private System.Windows.Forms.DataGridViewTextBoxColumn expire;
        private System.Windows.Forms.DataGridViewTextBoxColumn tId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn expire2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnUpdate2;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}
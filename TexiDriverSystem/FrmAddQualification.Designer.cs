﻿namespace TexiDriverSystem
{
    partial class FrmAddQualification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDes = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnQua = new System.Windows.Forms.Button();
            this.txtQualification = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.taxi_Driver_SystemDataSet = new TexiDriverSystem.Taxi_Driver_SystemDataSet();
            this.qualificationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qualificationsTableAdapter = new TexiDriverSystem.Taxi_Driver_SystemDataSetTableAdapters.QualificationsTableAdapter();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qualificationNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taxi_Driver_SystemDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qualificationsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label1.Location = new System.Drawing.Point(20, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Qualification:";
            // 
            // txtDes
            // 
            this.txtDes.Location = new System.Drawing.Point(116, 78);
            this.txtDes.Name = "txtDes";
            this.txtDes.Size = new System.Drawing.Size(213, 79);
            this.txtDes.TabIndex = 1;
            this.txtDes.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.label2.Location = new System.Drawing.Point(27, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Description:";
            // 
            // btnQua
            // 
            this.btnQua.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnQua.Location = new System.Drawing.Point(193, 163);
            this.btnQua.Name = "btnQua";
            this.btnQua.Size = new System.Drawing.Size(136, 37);
            this.btnQua.TabIndex = 2;
            this.btnQua.Text = "Save Qualification";
            this.btnQua.UseVisualStyleBackColor = true;
            this.btnQua.Click += new System.EventHandler(this.btnQua_Click);
            // 
            // txtQualification
            // 
            this.txtQualification.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.45F);
            this.txtQualification.Location = new System.Drawing.Point(116, 33);
            this.txtQualification.Name = "txtQualification";
            this.txtQualification.Size = new System.Drawing.Size(213, 23);
            this.txtQualification.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.qualificationNameDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.qualificationsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(352, 33);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(363, 167);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // taxi_Driver_SystemDataSet
            // 
            this.taxi_Driver_SystemDataSet.DataSetName = "Taxi_Driver_SystemDataSet";
            this.taxi_Driver_SystemDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // qualificationsBindingSource
            // 
            this.qualificationsBindingSource.DataMember = "Qualifications";
            this.qualificationsBindingSource.DataSource = this.taxi_Driver_SystemDataSet;
            // 
            // qualificationsTableAdapter
            // 
            this.qualificationsTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // qualificationNameDataGridViewTextBoxColumn
            // 
            this.qualificationNameDataGridViewTextBoxColumn.DataPropertyName = "QualificationName";
            this.qualificationNameDataGridViewTextBoxColumn.HeaderText = "QualificationName";
            this.qualificationNameDataGridViewTextBoxColumn.Name = "qualificationNameDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // FrmAddQualification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(745, 227);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtQualification);
            this.Controls.Add(this.btnQua);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDes);
            this.Controls.Add(this.label1);
            this.Name = "FrmAddQualification";
            this.Text = "Add Qualification";
            this.Load += new System.EventHandler(this.FrmAddQualification_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taxi_Driver_SystemDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qualificationsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtDes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnQua;
        private System.Windows.Forms.TextBox txtQualification;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Taxi_Driver_SystemDataSet taxi_Driver_SystemDataSet;
        private System.Windows.Forms.BindingSource qualificationsBindingSource;
        private Taxi_Driver_SystemDataSetTableAdapters.QualificationsTableAdapter qualificationsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qualificationNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
    }
}
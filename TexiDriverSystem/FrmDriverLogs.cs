﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using TexiDriverSystem.View_Models;

namespace TexiDriverSystem
{
    public partial class FrmDriverLogs : Form
    {
        public FrmDriverLogs()
        {
            InitializeComponent();
        }
        int dmilisec = 0;
        int dsec = 0;
        int dmin = 0;
        int dhour = 0;

        int jmilisec = 0;
        int jsec = 0;
        int jmin = 0;
        int jhour = 0;

        private void DayLogTimer_Tick(object sender, EventArgs e)
        {
            dmilisec++;
            if (dmilisec == 100)
            {
                dsec++;
                dmilisec = 0;
            }
            if (dsec == 60)
            {
                dmin++;
                dsec = 0;
            }
            if (dmin == 60)
            {
                dhour++;
                dmin = 0;
            }

            DayTime.Text = dhour.ToString() + " : " + dmin.ToString() + " : " + dsec.ToString() + " : " + dmilisec.ToString();
        }

        private void JourneyLogTimer_Tick(object sender, EventArgs e)
        {
            jmilisec++;
            if (jmilisec == 100)
            {
                jsec++;
                jmilisec = 0;
            }
            if (jsec == 60)
            {
                jmin++;
                jsec = 0;
            }
            if (jmin == 60)
            {
                jhour++;
                jmin = 0;
            }

            JourneyTime.Text = jhour.ToString() + " : " + jmin.ToString() + " : " + jsec.ToString() + " : " + jmilisec.ToString();
        }

        private void BtnStartDayLog_Click(object sender, EventArgs e)
        {
            var driverDayLog = new DriverDayLogs();
            driverDayLog.DriverId = Convert.ToInt64(LblDriverId.Text);
            driverDayLog.StartOfDay = DateTime.Now;
            driverDayLog.EndOfDay = DateTime.Now;
            var client = new HttpClient { BaseAddress = new Uri("http://localhost:60111") };
            var response = client.PostAsync("api/driverdaylogs", new StringContent(
   new JavaScriptSerializer().Serialize(driverDayLog), Encoding.UTF8, "application/json")).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync();
                DriverDayLogs driver = new JavaScriptSerializer().Deserialize<DriverDayLogs>(result.Result);
                LblDriverDayLogId.Text = driver.Id.ToString();
                BtnStartDayLog.Enabled = false;
                BtnEndDayLog.Enabled = true;
                BtnStartJourneyLog.Enabled = true;
                DayLogTimer.Start();
            }
            else
            {
                MessageBox.Show("There is an Error");
            }
        }

        private void BtnStartJourneyLog_Click(object sender, EventArgs e)
        {
            var driverJourneyLog = new DriverJourneyLogs();
            driverJourneyLog.DriverDayLogId = Convert.ToInt64(LblDriverDayLogId.Text);
            driverJourneyLog.StartOfJourney = DateTime.Now;
            driverJourneyLog.Status = 0;
            driverJourneyLog.EndOfJourney = DateTime.Now;
            var client = new HttpClient { BaseAddress = new Uri("http://localhost:60111") };
            var response = client.PostAsync("api/driverjourneylog", new StringContent(
   new JavaScriptSerializer().Serialize(driverJourneyLog), Encoding.UTF8, "application/json")).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync();
                List<DriverJourneyLogs> logs = new JavaScriptSerializer().Deserialize<List<DriverJourneyLogs>>(result.Result);
                List<DriverJourneyLogsVm> vm = new List<DriverJourneyLogsVm>();
                for (int i = 0; i < logs.Count; i++)
                {
                    DriverJourneyLogsVm insert = new DriverJourneyLogsVm();
                    insert.StartOfJourney = logs[i].StartOfJourney;
                    insert.EndOfJourney = logs[i].EndOfJourney;
                    TimeSpan diff = insert.EndOfJourney - insert.StartOfJourney;
                    if (logs[i].StartOfJourney == logs[i].EndOfJourney)
                    {
                        LblJourneyLogId.Text = logs[i].Id.ToString();
                    }
                    int hours = diff.Hours;
                    int min = diff.Minutes;
                    int seconds = diff.Seconds;
                    insert.JourneyDuration = hours + " : " + min + " : " + seconds;
                    vm.Insert(i, insert);
                    LblJourneyLogId.Text = logs[i].Id.ToString();
                }
                BtnStartJourneyLog.Enabled = false;
                BtnEndJourneyLog.Enabled = true;
                BtnEndDayLog.Enabled = false;
                DgvDetails.AutoGenerateColumns = false;
                DgvDetails.DataSource = vm;
                JourneyLogTimer.Start();
            }
            else
            {
                MessageBox.Show("There is an Error");
            }
        }

        private void BtnEndDayLog_Click(object sender, EventArgs e)
        {
            var driverDayLog = new DriverDayLogs();
            driverDayLog.Id = Convert.ToInt64(LblDriverDayLogId.Text);
            driverDayLog.EndOfDay = DateTime.Now;
            long Id = driverDayLog.Id;
            var client = new HttpClient { BaseAddress = new Uri("http://localhost:60111") };
            var response = client.PutAsync("api/driverdaylogs/" + Id, new StringContent(
   new JavaScriptSerializer().Serialize(driverDayLog), Encoding.UTF8, "application/json")).Result;
            if (response.IsSuccessStatusCode)
            {
                BtnStartDayLog.Enabled = true;
                BtnEndDayLog.Enabled = false;
                BtnStartJourneyLog.Enabled = false;
                BtnEndJourneyLog.Enabled = false;
                DayLogTimer.Stop();
                DayTime.Text = 0.ToString() + " : " + 0.ToString() + " : " + 0.ToString() + " : " + 0.ToString();
                MessageBox.Show("Todays Log Recorded");
            }
            else
            {
                MessageBox.Show("There is an Error");
            }
        }

        private void BtnEndJourneyLog_Click(object sender, EventArgs e)
        {
            var driverJourneyLog = new DriverJourneyLogs();
            driverJourneyLog.Id = Convert.ToInt64(LblJourneyLogId.Text);
            driverJourneyLog.EndOfJourney = DateTime.Now;
            var client = new HttpClient { BaseAddress = new Uri("http://localhost:60111") };
            var response = client.PutAsync("api/driverjourneylog/" + driverJourneyLog.Id, new StringContent(
   new JavaScriptSerializer().Serialize(driverJourneyLog), Encoding.UTF8, "application/json")).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync();
                List<DriverJourneyLogs> logs = new JavaScriptSerializer().Deserialize<List<DriverJourneyLogs>>(result.Result);
                List<DriverJourneyLogsVm> vm = new List<DriverJourneyLogsVm>();
                for (int i = 0; i < logs.Count; i++)
                {
                    DriverJourneyLogsVm insert = new DriverJourneyLogsVm();
                    insert.StartOfJourney = logs[i].StartOfJourney;
                    insert.EndOfJourney = logs[i].EndOfJourney;
                    TimeSpan diff = insert.EndOfJourney - insert.StartOfJourney;
                    if (logs[i].StartOfJourney == logs[i].EndOfJourney)
                    {
                        LblJourneyLogId.Text = logs[i].Id.ToString();
                    }
                    else
                    {
                        LblJourneyLogId.Text = "";
                    }
                    int hours = diff.Hours;
                    int min = diff.Minutes;
                    int seconds = diff.Seconds;
                    insert.JourneyDuration = hours + " : " + min + " : " + seconds;
                    vm.Insert(i, insert);
                }
                BtnEndDayLog.Enabled = true;
                BtnStartDayLog.Enabled = false;
                BtnEndJourneyLog.Enabled = false;
                BtnStartJourneyLog.Enabled = true;
                JourneyLogTimer.Stop();
                JourneyTime.Text = 0.ToString() + " : " + 0.ToString() + " : " + 0.ToString() + " : " + 0.ToString();
            }
            else
            {
                MessageBox.Show("There is an Error");
            }
        }
    }
}

﻿using System.Linq;
using System.Windows.Forms;

namespace TexiDriverSystem
{
    public partial class FrmAddQualification : Form
    {
        public FrmAddQualification()
        {
            InitializeComponent();
        }

        private void FrmAddQualification_Load(object sender, System.EventArgs e)
        {
            // TODO: This line of code loads data into the 'taxi_Driver_SystemDataSet.Qualifications' table. You can move, or remove it, as needed.
            qualificationsTableAdapter.Fill(taxi_Driver_SystemDataSet.Qualifications);
        }

        private void btnQua_Click(object sender, System.EventArgs e)
        {
            using (var db = new Taxi_Driver_SystemEntities())
            {
                if (btnQua.Text != @"Save Qualification")
                {
                    var row = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    var id = int.Parse(row);
                    var qualifications = db.Qualifications.FirstOrDefault(q => q.Id.Equals(id));
                    if (qualifications != null)
                    {
                        qualifications.Description = txtDes.Text;
                        qualifications.QualificationName = txtQualification.Text;
                    }
                }
                else
                {
                    var list = db.Qualifications.ToList();
                    var qua = new Qualifications
                    {
                        Id = list.Count + 1,
                        QualificationName = txtQualification.Text,
                        Description = txtDes.Text
                    };
                    db.Qualifications.Add(qua);   
                }
                db.SaveChanges();
            }

            qualificationsTableAdapter.Fill(taxi_Driver_SystemDataSet.Qualifications);
        }

        private void dataGridView1_SelectionChanged(object sender, System.EventArgs e)
        {
//            var row = dataGridView1.SelectedRows[0].Cells;
//            txtQualification.Text = row[1].ToString();
//            txtDes.Text = row[2].ToString();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            btnQua.Text = @"Update Trainings";
            var row = dataGridView1.SelectedRows[0].Cells;
            txtQualification.Text = row[1].Value.ToString();
            txtDes.Text = row[2].Value.ToString();
        }
    }
}
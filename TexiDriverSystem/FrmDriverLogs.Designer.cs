﻿namespace TexiDriverSystem
{
    partial class FrmDriverLogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BtnStartDayLog = new System.Windows.Forms.Button();
            this.BtnEndDayLog = new System.Windows.Forms.Button();
            this.BtnStartJourneyLog = new System.Windows.Forms.Button();
            this.DgvDetails = new System.Windows.Forms.DataGridView();
            this.BtnEndJourneyLog = new System.Windows.Forms.Button();
            this.DayTime = new System.Windows.Forms.Label();
            this.JourneyTime = new System.Windows.Forms.Label();
            this.DayLogTimer = new System.Windows.Forms.Timer(this.components);
            this.JourneyLogTimer = new System.Windows.Forms.Timer(this.components);
            this.LblDriverMobile = new System.Windows.Forms.Label();
            this.LblDriverDob = new System.Windows.Forms.Label();
            this.LblDriverName = new System.Windows.Forms.Label();
            this.LblDriverId = new System.Windows.Forms.Label();
            this.LblDriverDayLogId = new System.Windows.Forms.Label();
            this.StartOfJourney = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndOfJourney = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JourneyDuration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LblJourneyLogId = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgvDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnStartDayLog
            // 
            this.BtnStartDayLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnStartDayLog.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnStartDayLog.FlatAppearance.BorderSize = 0;
            this.BtnStartDayLog.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnStartDayLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnStartDayLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStartDayLog.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnStartDayLog.Location = new System.Drawing.Point(78, 69);
            this.BtnStartDayLog.Name = "BtnStartDayLog";
            this.BtnStartDayLog.Size = new System.Drawing.Size(328, 50);
            this.BtnStartDayLog.TabIndex = 1;
            this.BtnStartDayLog.Text = "Click Me to Start Day Log";
            this.BtnStartDayLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnStartDayLog.UseVisualStyleBackColor = false;
            this.BtnStartDayLog.Click += new System.EventHandler(this.BtnStartDayLog_Click);
            // 
            // BtnEndDayLog
            // 
            this.BtnEndDayLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnEndDayLog.Enabled = false;
            this.BtnEndDayLog.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnEndDayLog.FlatAppearance.BorderSize = 0;
            this.BtnEndDayLog.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnEndDayLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEndDayLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEndDayLog.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnEndDayLog.Location = new System.Drawing.Point(412, 69);
            this.BtnEndDayLog.Name = "BtnEndDayLog";
            this.BtnEndDayLog.Size = new System.Drawing.Size(313, 50);
            this.BtnEndDayLog.TabIndex = 1;
            this.BtnEndDayLog.Text = "Click Me to End Day Log";
            this.BtnEndDayLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnEndDayLog.UseVisualStyleBackColor = false;
            this.BtnEndDayLog.Click += new System.EventHandler(this.BtnEndDayLog_Click);
            // 
            // BtnStartJourneyLog
            // 
            this.BtnStartJourneyLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnStartJourneyLog.Enabled = false;
            this.BtnStartJourneyLog.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnStartJourneyLog.FlatAppearance.BorderSize = 0;
            this.BtnStartJourneyLog.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnStartJourneyLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnStartJourneyLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStartJourneyLog.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnStartJourneyLog.Location = new System.Drawing.Point(78, 193);
            this.BtnStartJourneyLog.Name = "BtnStartJourneyLog";
            this.BtnStartJourneyLog.Size = new System.Drawing.Size(647, 50);
            this.BtnStartJourneyLog.TabIndex = 1;
            this.BtnStartJourneyLog.Text = "Click Me To Start Jouney Log";
            this.BtnStartJourneyLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnStartJourneyLog.UseVisualStyleBackColor = false;
            this.BtnStartJourneyLog.Click += new System.EventHandler(this.BtnStartJourneyLog_Click);
            // 
            // DgvDetails
            // 
            this.DgvDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvDetails.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StartOfJourney,
            this.EndOfJourney,
            this.JourneyDuration});
            this.DgvDetails.Location = new System.Drawing.Point(78, 307);
            this.DgvDetails.Name = "DgvDetails";
            this.DgvDetails.Size = new System.Drawing.Size(647, 150);
            this.DgvDetails.TabIndex = 2;
            // 
            // BtnEndJourneyLog
            // 
            this.BtnEndJourneyLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnEndJourneyLog.Enabled = false;
            this.BtnEndJourneyLog.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.BtnEndJourneyLog.FlatAppearance.BorderSize = 0;
            this.BtnEndJourneyLog.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnEndJourneyLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEndJourneyLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEndJourneyLog.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnEndJourneyLog.Location = new System.Drawing.Point(397, 463);
            this.BtnEndJourneyLog.Name = "BtnEndJourneyLog";
            this.BtnEndJourneyLog.Size = new System.Drawing.Size(328, 34);
            this.BtnEndJourneyLog.TabIndex = 1;
            this.BtnEndJourneyLog.Text = "Click Me to End Journey Log";
            this.BtnEndJourneyLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnEndJourneyLog.UseVisualStyleBackColor = false;
            this.BtnEndJourneyLog.Click += new System.EventHandler(this.BtnEndJourneyLog_Click);
            // 
            // DayTime
            // 
            this.DayTime.AutoSize = true;
            this.DayTime.BackColor = System.Drawing.Color.White;
            this.DayTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DayTime.Location = new System.Drawing.Point(308, 138);
            this.DayTime.Name = "DayTime";
            this.DayTime.Size = new System.Drawing.Size(192, 42);
            this.DayTime.TabIndex = 3;
            this.DayTime.Text = "0 : 0 : 0 : 0";
            // 
            // JourneyTime
            // 
            this.JourneyTime.AutoSize = true;
            this.JourneyTime.BackColor = System.Drawing.Color.White;
            this.JourneyTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JourneyTime.Location = new System.Drawing.Point(533, 262);
            this.JourneyTime.Name = "JourneyTime";
            this.JourneyTime.Size = new System.Drawing.Size(192, 42);
            this.JourneyTime.TabIndex = 3;
            this.JourneyTime.Text = "0 : 0 : 0 : 0";
            // 
            // DayLogTimer
            // 
            this.DayLogTimer.Tick += new System.EventHandler(this.DayLogTimer_Tick);
            // 
            // JourneyLogTimer
            // 
            this.JourneyLogTimer.Tick += new System.EventHandler(this.JourneyLogTimer_Tick);
            // 
            // LblDriverMobile
            // 
            this.LblDriverMobile.AutoSize = true;
            this.LblDriverMobile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverMobile.ForeColor = System.Drawing.Color.Red;
            this.LblDriverMobile.Location = new System.Drawing.Point(626, 31);
            this.LblDriverMobile.Name = "LblDriverMobile";
            this.LblDriverMobile.Size = new System.Drawing.Size(96, 20);
            this.LblDriverMobile.TabIndex = 4;
            this.LblDriverMobile.Text = "Qualification";
            // 
            // LblDriverDob
            // 
            this.LblDriverDob.AutoSize = true;
            this.LblDriverDob.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverDob.ForeColor = System.Drawing.Color.Red;
            this.LblDriverDob.Location = new System.Drawing.Point(424, 31);
            this.LblDriverDob.Name = "LblDriverDob";
            this.LblDriverDob.Size = new System.Drawing.Size(96, 20);
            this.LblDriverDob.TabIndex = 5;
            this.LblDriverDob.Text = "Qualification";
            // 
            // LblDriverName
            // 
            this.LblDriverName.AutoSize = true;
            this.LblDriverName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverName.ForeColor = System.Drawing.Color.Red;
            this.LblDriverName.Location = new System.Drawing.Point(222, 31);
            this.LblDriverName.Name = "LblDriverName";
            this.LblDriverName.Size = new System.Drawing.Size(96, 20);
            this.LblDriverName.TabIndex = 6;
            this.LblDriverName.Text = "Qualification";
            // 
            // LblDriverId
            // 
            this.LblDriverId.AutoSize = true;
            this.LblDriverId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverId.ForeColor = System.Drawing.Color.Red;
            this.LblDriverId.Location = new System.Drawing.Point(37, 31);
            this.LblDriverId.Name = "LblDriverId";
            this.LblDriverId.Size = new System.Drawing.Size(96, 20);
            this.LblDriverId.TabIndex = 6;
            this.LblDriverId.Text = "Qualification";
            // 
            // LblDriverDayLogId
            // 
            this.LblDriverDayLogId.AutoSize = true;
            this.LblDriverDayLogId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverDayLogId.ForeColor = System.Drawing.Color.Black;
            this.LblDriverDayLogId.Location = new System.Drawing.Point(37, 160);
            this.LblDriverDayLogId.Name = "LblDriverDayLogId";
            this.LblDriverDayLogId.Size = new System.Drawing.Size(96, 20);
            this.LblDriverDayLogId.TabIndex = 6;
            this.LblDriverDayLogId.Text = "Qualification";
            // 
            // StartOfJourney
            // 
            this.StartOfJourney.DataPropertyName = "StartOfJourney";
            this.StartOfJourney.HeaderText = "Journey Start Time";
            this.StartOfJourney.Name = "StartOfJourney";
            // 
            // EndOfJourney
            // 
            this.EndOfJourney.DataPropertyName = "EndOfJourney";
            this.EndOfJourney.HeaderText = "Journey End Time";
            this.EndOfJourney.Name = "EndOfJourney";
            // 
            // JourneyDuration
            // 
            this.JourneyDuration.DataPropertyName = "JourneyDuration";
            this.JourneyDuration.HeaderText = "Journey Duration";
            this.JourneyDuration.Name = "JourneyDuration";
            // 
            // LblJourneyLogId
            // 
            this.LblJourneyLogId.AutoSize = true;
            this.LblJourneyLogId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblJourneyLogId.ForeColor = System.Drawing.Color.Black;
            this.LblJourneyLogId.Location = new System.Drawing.Point(37, 281);
            this.LblJourneyLogId.Name = "LblJourneyLogId";
            this.LblJourneyLogId.Size = new System.Drawing.Size(96, 20);
            this.LblJourneyLogId.TabIndex = 6;
            this.LblJourneyLogId.Text = "Qualification";
            // 
            // FrmDriverLogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 509);
            this.Controls.Add(this.LblDriverMobile);
            this.Controls.Add(this.LblDriverDob);
            this.Controls.Add(this.LblJourneyLogId);
            this.Controls.Add(this.LblDriverDayLogId);
            this.Controls.Add(this.LblDriverId);
            this.Controls.Add(this.LblDriverName);
            this.Controls.Add(this.JourneyTime);
            this.Controls.Add(this.DayTime);
            this.Controls.Add(this.DgvDetails);
            this.Controls.Add(this.BtnEndDayLog);
            this.Controls.Add(this.BtnStartJourneyLog);
            this.Controls.Add(this.BtnEndJourneyLog);
            this.Controls.Add(this.BtnStartDayLog);
            this.Name = "FrmDriverLogs";
            this.Text = "Driver Logs";
            ((System.ComponentModel.ISupportInitialize)(this.DgvDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnStartDayLog;
        private System.Windows.Forms.Button BtnEndDayLog;
        private System.Windows.Forms.Button BtnStartJourneyLog;
        private System.Windows.Forms.DataGridView DgvDetails;
        private System.Windows.Forms.Button BtnEndJourneyLog;
        private System.Windows.Forms.Label DayTime;
        private System.Windows.Forms.Label JourneyTime;
        private System.Windows.Forms.Timer DayLogTimer;
        private System.Windows.Forms.Timer JourneyLogTimer;
        public System.Windows.Forms.Label LblDriverMobile;
        public System.Windows.Forms.Label LblDriverDob;
        public System.Windows.Forms.Label LblDriverName;
        public System.Windows.Forms.Label LblDriverId;
        public System.Windows.Forms.Label LblDriverDayLogId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartOfJourney;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndOfJourney;
        private System.Windows.Forms.DataGridViewTextBoxColumn JourneyDuration;
        public System.Windows.Forms.Label LblJourneyLogId;
    }
}
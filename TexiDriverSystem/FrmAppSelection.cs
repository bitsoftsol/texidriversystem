﻿using System;
using System.Windows.Forms;

namespace TexiDriverSystem
{
    public partial class FrmAppSelection : Form
    {
        public FrmAppSelection()
        {
            InitializeComponent();
        }

        private void BtnDriverApp_Click(object sender, EventArgs e)
        {
            FrmDriverLogin frmDriverLogin = new FrmDriverLogin();
            Hide();
            frmDriverLogin.Show();
        }

        private void BtnAdminApp_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            this.Hide();
            frmLogin.Show();
        }
    }
}

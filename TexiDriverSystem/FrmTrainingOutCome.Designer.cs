﻿namespace TexiDriverSystem
{
    partial class FrmTrainingOutCome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DgvSessionTraining = new System.Windows.Forms.DataGridView();
            this.TxtSessionName = new System.Windows.Forms.TextBox();
            this.DtpSessionEndDate = new System.Windows.Forms.DateTimePicker();
            this.CmbTrainingOutCome = new System.Windows.Forms.ComboBox();
            this.DtpTrainingExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTrainingName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDriverName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpSessionStartDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnEdit = new System.Windows.Forms.Button();
            this.LblId = new System.Windows.Forms.Label();
            this.lblTrainingId = new System.Windows.Forms.Label();
            this.LblDriverId = new System.Windows.Forms.Label();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SessionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DriverName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SessionStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DriverId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSessionTraining)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvSessionTraining
            // 
            this.DgvSessionTraining.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvSessionTraining.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvSessionTraining.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.SessionName,
            this.TrainingName,
            this.DriverName,
            this.SessionStartDate,
            this.DriverId,
            this.TrainingId});
            this.DgvSessionTraining.Location = new System.Drawing.Point(429, 29);
            this.DgvSessionTraining.Name = "DgvSessionTraining";
            this.DgvSessionTraining.Size = new System.Drawing.Size(444, 506);
            this.DgvSessionTraining.TabIndex = 0;
            this.DgvSessionTraining.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvSessionTraining_CellClick);
            // 
            // TxtSessionName
            // 
            this.TxtSessionName.Location = new System.Drawing.Point(55, 59);
            this.TxtSessionName.Name = "TxtSessionName";
            this.TxtSessionName.ReadOnly = true;
            this.TxtSessionName.Size = new System.Drawing.Size(306, 20);
            this.TxtSessionName.TabIndex = 1;
            // 
            // DtpSessionEndDate
            // 
            this.DtpSessionEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpSessionEndDate.Location = new System.Drawing.Point(55, 301);
            this.DtpSessionEndDate.Name = "DtpSessionEndDate";
            this.DtpSessionEndDate.Size = new System.Drawing.Size(306, 20);
            this.DtpSessionEndDate.TabIndex = 2;
            // 
            // CmbTrainingOutCome
            // 
            this.CmbTrainingOutCome.FormattingEnabled = true;
            this.CmbTrainingOutCome.Items.AddRange(new object[] {
            "Pass",
            "Fail"});
            this.CmbTrainingOutCome.Location = new System.Drawing.Point(55, 428);
            this.CmbTrainingOutCome.Name = "CmbTrainingOutCome";
            this.CmbTrainingOutCome.Size = new System.Drawing.Size(306, 21);
            this.CmbTrainingOutCome.TabIndex = 3;
            // 
            // DtpTrainingExpiryDate
            // 
            this.DtpTrainingExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpTrainingExpiryDate.Location = new System.Drawing.Point(55, 361);
            this.DtpTrainingExpiryDate.Name = "DtpTrainingExpiryDate";
            this.DtpTrainingExpiryDate.Size = new System.Drawing.Size(306, 20);
            this.DtpTrainingExpiryDate.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(54, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Session Name";
            // 
            // TxtTrainingName
            // 
            this.TxtTrainingName.Location = new System.Drawing.Point(55, 122);
            this.TxtTrainingName.Name = "TxtTrainingName";
            this.TxtTrainingName.ReadOnly = true;
            this.TxtTrainingName.Size = new System.Drawing.Size(306, 20);
            this.TxtTrainingName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(51, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Training Name";
            // 
            // TxtDriverName
            // 
            this.TxtDriverName.Location = new System.Drawing.Point(55, 183);
            this.TxtDriverName.Name = "TxtDriverName";
            this.TxtDriverName.ReadOnly = true;
            this.TxtDriverName.Size = new System.Drawing.Size(306, 20);
            this.TxtDriverName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(48, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Driver Name";
            // 
            // DtpSessionStartDate
            // 
            this.DtpSessionStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpSessionStartDate.Location = new System.Drawing.Point(55, 239);
            this.DtpSessionStartDate.Name = "DtpSessionStartDate";
            this.DtpSessionStartDate.Size = new System.Drawing.Size(306, 20);
            this.DtpSessionStartDate.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(48, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Session Start Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(54, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Session End Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(51, 338);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Training Expiry Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(51, 405);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Training Outcome";
            // 
            // BtnEdit
            // 
            this.BtnEdit.Enabled = false;
            this.BtnEdit.Location = new System.Drawing.Point(157, 478);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(96, 34);
            this.BtnEdit.TabIndex = 5;
            this.BtnEdit.Text = "Edit";
            this.BtnEdit.UseVisualStyleBackColor = true;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // LblId
            // 
            this.LblId.AutoSize = true;
            this.LblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblId.ForeColor = System.Drawing.Color.Red;
            this.LblId.Location = new System.Drawing.Point(54, 16);
            this.LblId.Name = "LblId";
            this.LblId.Size = new System.Drawing.Size(0, 20);
            this.LblId.TabIndex = 4;
            // 
            // lblTrainingId
            // 
            this.lblTrainingId.AutoSize = true;
            this.lblTrainingId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrainingId.ForeColor = System.Drawing.Color.Red;
            this.lblTrainingId.Location = new System.Drawing.Point(168, 99);
            this.lblTrainingId.Name = "lblTrainingId";
            this.lblTrainingId.Size = new System.Drawing.Size(0, 20);
            this.lblTrainingId.TabIndex = 4;
            // 
            // LblDriverId
            // 
            this.LblDriverId.AutoSize = true;
            this.LblDriverId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverId.ForeColor = System.Drawing.Color.Red;
            this.LblDriverId.Location = new System.Drawing.Point(150, 160);
            this.LblDriverId.Name = "LblDriverId";
            this.LblDriverId.Size = new System.Drawing.Size(0, 20);
            this.LblDriverId.TabIndex = 4;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // SessionName
            // 
            this.SessionName.DataPropertyName = "SessionName";
            this.SessionName.HeaderText = "Session Name";
            this.SessionName.Name = "SessionName";
            // 
            // TrainingName
            // 
            this.TrainingName.DataPropertyName = "CourseName";
            this.TrainingName.HeaderText = "Trainig Name";
            this.TrainingName.Name = "TrainingName";
            // 
            // DriverName
            // 
            this.DriverName.DataPropertyName = "DriverName";
            this.DriverName.HeaderText = "Driver Name";
            this.DriverName.Name = "DriverName";
            // 
            // SessionStartDate
            // 
            this.SessionStartDate.DataPropertyName = "SessionStartDate";
            this.SessionStartDate.HeaderText = "Session Start Date";
            this.SessionStartDate.Name = "SessionStartDate";
            // 
            // DriverId
            // 
            this.DriverId.DataPropertyName = "DriverId";
            this.DriverId.HeaderText = "DriverId";
            this.DriverId.Name = "DriverId";
            this.DriverId.ReadOnly = true;
            this.DriverId.Visible = false;
            // 
            // TrainingId
            // 
            this.TrainingId.DataPropertyName = "TrainingId";
            this.TrainingId.HeaderText = "TrainingId";
            this.TrainingId.Name = "TrainingId";
            this.TrainingId.ReadOnly = true;
            this.TrainingId.Visible = false;
            // 
            // FrmTrainingOutCome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 547);
            this.Controls.Add(this.BtnEdit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LblDriverId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTrainingId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LblId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CmbTrainingOutCome);
            this.Controls.Add(this.TxtDriverName);
            this.Controls.Add(this.DtpTrainingExpiryDate);
            this.Controls.Add(this.TxtTrainingName);
            this.Controls.Add(this.DtpSessionStartDate);
            this.Controls.Add(this.DtpSessionEndDate);
            this.Controls.Add(this.TxtSessionName);
            this.Controls.Add(this.DgvSessionTraining);
            this.Name = "FrmTrainingOutCome";
            this.Text = "Record Training OutCome";
            ((System.ComponentModel.ISupportInitialize)(this.DgvSessionTraining)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvSessionTraining;
        private System.Windows.Forms.TextBox TxtSessionName;
        private System.Windows.Forms.DateTimePicker DtpSessionEndDate;
        private System.Windows.Forms.ComboBox CmbTrainingOutCome;
        private System.Windows.Forms.DateTimePicker DtpTrainingExpiryDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTrainingName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtDriverName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DtpSessionStartDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.Label LblId;
        private System.Windows.Forms.Label lblTrainingId;
        private System.Windows.Forms.Label LblDriverId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn SessionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DriverName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SessionStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DriverId;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingId;
    }
}
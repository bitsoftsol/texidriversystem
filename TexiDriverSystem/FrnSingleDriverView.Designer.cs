﻿namespace TexiDriverSystem
{
    partial class FrnSingleDriverView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DgvQualification = new System.Windows.Forms.DataGridView();
            this.DgvTraining = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualificationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualificationExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualificationType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LicenseNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GeographicalTests = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingSessionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainigSessionStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LblDriverName = new System.Windows.Forms.Label();
            this.LblDriverDob = new System.Windows.Forms.Label();
            this.LblDriverMobile = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgvQualification)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTraining)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvQualification
            // 
            this.DgvQualification.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvQualification.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvQualification.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvQualification.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.QualificationName,
            this.QualificationExpiryDate,
            this.QualificationType,
            this.LicenseNumber,
            this.GeographicalTests});
            this.DgvQualification.Location = new System.Drawing.Point(90, 81);
            this.DgvQualification.Name = "DgvQualification";
            this.DgvQualification.Size = new System.Drawing.Size(587, 150);
            this.DgvQualification.TabIndex = 0;
            // 
            // DgvTraining
            // 
            this.DgvTraining.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvTraining.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvTraining.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTraining.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tid,
            this.TrainingName,
            this.TrainingExpiryDate,
            this.TrainingSessionName,
            this.TrainigSessionStatus});
            this.DgvTraining.Location = new System.Drawing.Point(90, 280);
            this.DgvTraining.Name = "DgvTraining";
            this.DgvTraining.Size = new System.Drawing.Size(587, 150);
            this.DgvTraining.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(86, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Qualification";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(86, 257);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Training";
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // QualificationName
            // 
            this.QualificationName.DataPropertyName = "QualificationName";
            this.QualificationName.HeaderText = "Qualification Name";
            this.QualificationName.Name = "QualificationName";
            // 
            // QualificationExpiryDate
            // 
            this.QualificationExpiryDate.DataPropertyName = "QualificationExpiryDate";
            this.QualificationExpiryDate.HeaderText = "Qualification Expiry Date";
            this.QualificationExpiryDate.Name = "QualificationExpiryDate";
            // 
            // QualificationType
            // 
            this.QualificationType.DataPropertyName = "QualificationType";
            this.QualificationType.HeaderText = "Qualification Type";
            this.QualificationType.Name = "QualificationType";
            // 
            // LicenseNumber
            // 
            this.LicenseNumber.DataPropertyName = "LicenseNumber";
            this.LicenseNumber.HeaderText = "License Number";
            this.LicenseNumber.Name = "LicenseNumber";
            // 
            // GeographicalTests
            // 
            this.GeographicalTests.DataPropertyName = "GeographicalTests";
            this.GeographicalTests.HeaderText = "Geographical Tests";
            this.GeographicalTests.Name = "GeographicalTests";
            // 
            // Tid
            // 
            this.Tid.DataPropertyName = "Id";
            this.Tid.HeaderText = "Id";
            this.Tid.Name = "Tid";
            this.Tid.ReadOnly = true;
            this.Tid.Visible = false;
            // 
            // TrainingName
            // 
            this.TrainingName.DataPropertyName = "TrainingName";
            this.TrainingName.HeaderText = "Training Name";
            this.TrainingName.Name = "TrainingName";
            // 
            // TrainingExpiryDate
            // 
            this.TrainingExpiryDate.DataPropertyName = "TrainingExpiryDate";
            this.TrainingExpiryDate.HeaderText = "Training Expiry Date";
            this.TrainingExpiryDate.Name = "TrainingExpiryDate";
            // 
            // TrainingSessionName
            // 
            this.TrainingSessionName.DataPropertyName = "TrainingSessionName";
            this.TrainingSessionName.HeaderText = "Training Session Name";
            this.TrainingSessionName.Name = "TrainingSessionName";
            // 
            // TrainigSessionStatus
            // 
            this.TrainigSessionStatus.DataPropertyName = "TrainigSessionStatus";
            this.TrainigSessionStatus.HeaderText = "Training Session Status";
            this.TrainigSessionStatus.Name = "TrainigSessionStatus";
            // 
            // LblDriverName
            // 
            this.LblDriverName.AutoSize = true;
            this.LblDriverName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverName.ForeColor = System.Drawing.Color.Red;
            this.LblDriverName.Location = new System.Drawing.Point(86, 9);
            this.LblDriverName.Name = "LblDriverName";
            this.LblDriverName.Size = new System.Drawing.Size(96, 20);
            this.LblDriverName.TabIndex = 2;
            this.LblDriverName.Text = "Qualification";
            // 
            // LblDriverDob
            // 
            this.LblDriverDob.AutoSize = true;
            this.LblDriverDob.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverDob.ForeColor = System.Drawing.Color.Red;
            this.LblDriverDob.Location = new System.Drawing.Point(333, 9);
            this.LblDriverDob.Name = "LblDriverDob";
            this.LblDriverDob.Size = new System.Drawing.Size(96, 20);
            this.LblDriverDob.TabIndex = 2;
            this.LblDriverDob.Text = "Qualification";
            // 
            // LblDriverMobile
            // 
            this.LblDriverMobile.AutoSize = true;
            this.LblDriverMobile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDriverMobile.ForeColor = System.Drawing.Color.Red;
            this.LblDriverMobile.Location = new System.Drawing.Point(565, 9);
            this.LblDriverMobile.Name = "LblDriverMobile";
            this.LblDriverMobile.Size = new System.Drawing.Size(96, 20);
            this.LblDriverMobile.TabIndex = 2;
            this.LblDriverMobile.Text = "Qualification";
            // 
            // FrnSingleDriverView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 482);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LblDriverMobile);
            this.Controls.Add(this.LblDriverDob);
            this.Controls.Add(this.LblDriverName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DgvTraining);
            this.Controls.Add(this.DgvQualification);
            this.Name = "FrnSingleDriverView";
            this.Text = "FrnSingleDriverView";
            ((System.ComponentModel.ISupportInitialize)(this.DgvQualification)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTraining)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView DgvQualification;
        public System.Windows.Forms.DataGridView DgvTraining;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualificationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualificationExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualificationType;
        private System.Windows.Forms.DataGridViewTextBoxColumn LicenseNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn GeographicalTests;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tid;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingSessionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainigSessionStatus;
        public System.Windows.Forms.Label LblDriverName;
        public System.Windows.Forms.Label LblDriverDob;
        public System.Windows.Forms.Label LblDriverMobile;
    }
}
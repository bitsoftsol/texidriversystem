﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace TexiDriverSystem
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtUserName.Text) || string.IsNullOrEmpty(TxtPass.Text))
            {
                if (string.IsNullOrEmpty(TxtUserName.Text))
                {
                    LblPassMessage.Text = "";
                    LblMessage.Text = "Please Enter Your User Name";
                    TxtUserName.Focus();
                }
                if (string.IsNullOrEmpty(TxtPass.Text))
                {
                    LblMessage.Text = "";
                    LblPassMessage.Text = "Please Enter Your Password";
                }

                if (!string.IsNullOrEmpty(TxtUserName.Text) || !string.IsNullOrEmpty(TxtPass.Text)) return;
                LblMessage.Text = @"Please Enter Your User Name";
                LblPassMessage.Text = "Please Enter Your Password";
                TxtUserName.Focus();
                return;
            }
            LblMessage.Text = "";
            LblPassMessage.Text = "";
            try
            {
                using (var db = new Taxi_Driver_SystemEntities())
                {
                    var user = db.Users.SingleOrDefault(f => (f.UserName == TxtUserName.Text || f.Email == TxtUserName.Text) && f.Password == TxtPass.Text);
                    if (user != null)
                    {

                        var main = new frmMain();
                        Hide();
                        main.Show();
                    }
                    else
                    {
                        LblFormMessage.Text = @"Your Username or Password is Incorrect";
                    }
                }
            }
            catch (Exception ex)
            {
                LblFormMessage.Text = ex.Message;
            }

        }
    }
}

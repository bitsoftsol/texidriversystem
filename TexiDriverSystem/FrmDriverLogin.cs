﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace TexiDriverSystem
{
    public partial class FrmDriverLogin : Form
    {
        public FrmDriverLogin()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtMobile.Text))
            {
                MessageBox.Show("Please Enter Your Mobile Number to Login");
                TxtMobile.Focus();
                return;
            }
            try
            {
                using (var db = new Taxi_Driver_SystemEntities())
                {
                    var user = db.Drivers.SingleOrDefault(f => (f.Mobile == TxtMobile.Text));
                    if (user != null)
                    {
                        FrmDriverLogs frmDriverLogs = new FrmDriverLogs();
                        Hide();
                        frmDriverLogs.LblDriverId.Text = user.Id.ToString();
                        frmDriverLogs.LblDriverName.Text = user.FirstName;
                        frmDriverLogs.LblDriverDob.Text = user.Dob.ToString();
                        frmDriverLogs.LblDriverMobile.Text = user.Mobile;
                        frmDriverLogs.Show();
                    }
                    else
                    {
                        MessageBox.Show("Invalid Input");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TexiDriverSystem.View_Models;

namespace TexiDriverSystem
{
    public partial class FrmTrainingOutCome : Form
    {
        public FrmTrainingOutCome()
        {
            InitializeComponent();
            OnLoad();
        }
        private void OnLoad()
        {
            var trainingOutcome = new List<TrainingOutCome>();
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                var trainingSessions = db.TrainingSessions.Where(r => r.Status == 0).ToList();
                for (int i = 0; i < trainingSessions.Count; i++)
                {
                    var insertTrainingOutcome = new TrainingOutCome();
                    insertTrainingOutcome.Id = trainingSessions[i].Id;
                    insertTrainingOutcome.SessionName = trainingSessions[i].SessionName;
                    insertTrainingOutcome.SessionStartDate = trainingSessions[i].SessionStartDate;
                    long id = trainingSessions[i].TrainingId;
                    var training = db.Trainings.SingleOrDefault(r => r.Id == id);
                    insertTrainingOutcome.CourseName = training != null ? training.CourseName : "None";
                    long driverId = trainingSessions[i].DriverId;
                    insertTrainingOutcome.DriverName = db.Drivers.SingleOrDefault(r => r.Id == driverId) != null ? db.Drivers.SingleOrDefault(r => r.Id == driverId).FirstName : "None";
                    trainingOutcome.Insert(i, insertTrainingOutcome);
                }
            }
            DgvSessionTraining.AutoGenerateColumns = false;
            DgvSessionTraining.DataSource = trainingOutcome;
        }

        private void DgvSessionTraining_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null)
            {
                return;
            }
            if (dgv.CurrentRow.Selected)
            {
                TxtDriverName.Text = dgv.Rows[e.RowIndex].Cells["DriverName"].Value.ToString();
                TxtSessionName.Text = dgv.Rows[e.RowIndex].Cells["SessionName"].Value.ToString();
                TxtTrainingName.Text = dgv.Rows[e.RowIndex].Cells["TrainingName"].Value.ToString();
                DtpSessionStartDate.Value = Convert.ToDateTime(dgv.Rows[e.RowIndex].Cells["SessionStartDate"].Value);
                LblId.Text = dgv.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                lblTrainingId.Text = dgv.Rows[e.RowIndex].Cells["TrainingId"].Value.ToString();
                LblDriverId.Text = dgv.Rows[e.RowIndex].Cells["DriverId"].Value.ToString();
                BtnEdit.Enabled = true;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            long id = Convert.ToInt64(LblId.Text);
            long driverId = Convert.ToInt64(LblDriverId.Text);
            long trainingId = Convert.ToInt64(lblTrainingId.Text);
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                var session = db.TrainingSessions.SingleOrDefault(r => r.Id == id);
                if (session != null)
                {
                    session.SessionEndDate = DtpSessionEndDate.Value;
                    session.Status = CmbTrainingOutCome.SelectedText == "Pass" ? 1 : 2;
                    db.SaveChanges();
                }
                var driverTraining = new DriverTrainings();
                var newId = db.DriverTrainings.OrderByDescending(r => r.Id).FirstOrDefault();
                driverTraining.Id = newId == null ? 1 : newId.Id + 1;
                driverTraining.DriverId = session.DriverId;
                driverTraining.DriverSessionTrainingId = session.Id;
                driverTraining.TrainingExpiryDate = DtpTrainingExpiryDate.Value;
                driverTraining.TrainingId = session.TrainingId;
                db.DriverTrainings.Add(driverTraining);
                db.SaveChanges();
            }
            MessageBox.Show("Data Updated Successfully");
        }
    }
}

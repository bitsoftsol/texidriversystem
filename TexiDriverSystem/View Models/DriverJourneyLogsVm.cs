﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TexiDriverSystem.View_Models
{
   public class DriverJourneyLogsVm
    {
        public DateTime StartOfJourney { get; set; }
        public DateTime EndOfJourney { get; set; }
        public string JourneyDuration { get; set; }
    }
}

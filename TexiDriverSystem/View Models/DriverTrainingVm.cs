﻿using System;

namespace TexiDriverSystem.View_Models
{
    public class DriverTrainingVm
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime TrainingExpiryDate { get; set; }
        public string TrainingName { get; set; }
        public string TrainingSessionName { get; set; }
        public string TrainingSessionStatus { get; set; }

    }
}

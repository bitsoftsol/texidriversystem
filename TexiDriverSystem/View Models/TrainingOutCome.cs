﻿using System;

namespace TexiDriverSystem.View_Models
{
    public class TrainingOutCome
    {
        public long Id { get; set; }
        public string SessionName { get; set; }
        public long TrainingId { get; set; }
        public string CourseName { get; set; }
        public long DriverId { get; set; }
        public string DriverName { get; set; }
        public DateTime SessionStartDate { get; set; }
    }
}

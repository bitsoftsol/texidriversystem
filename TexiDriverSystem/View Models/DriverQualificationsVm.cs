﻿using System;

namespace TexiDriverSystem.View_Models
{
    public class DriverQualificationsVm
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LicenseNumber { get; set; }
        public string GeographicalTests { get; set; }
        public DateTime QualificationExpiryDate { get; set; }
        public string QualificationName { get; set; }
        public string QualificationType { get; set; }
    }
}

﻿using System;

namespace TexiDriverSystem.View_Models
{
    public class JourneyFeedbackVm
    {
        public long Id { get; set; }
        public string DriverName { get; set; }
        public DateTime StartOfDay { get; set; }
        public DateTime StartOfJourney { get; set; }
        public DateTime EndOfJourney { get; set; }
        public string Duration { get; set; }
    }
}

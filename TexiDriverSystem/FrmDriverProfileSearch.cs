﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TexiDriverSystem.View_Models;

namespace TexiDriverSystem
{
    public partial class FrmDriverProfileSearch : Form
    {
        public FrmDriverProfileSearch()
        {
            InitializeComponent();
            OnLoad();
        }

        private void OnLoad()
        {
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                DgvDrivers.AutoGenerateColumns = false;
                DgvDrivers.DataSource = db.Drivers.ToList();
            }
        }

        private void TxtSerach_KeyUp(object sender, KeyEventArgs e)
        {
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                string search = TxtSerach.Text;
                DgvDrivers.DataSource = db.Drivers.Where(t => t.FirstName.Contains(search) || t.LastName.Contains(search)).ToList();
            }
        }

        private void DgvDrivers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null)
            {
                return;
            }
            if (dgv.CurrentRow.Selected)
            {
                LblDriverId.Text = dgv.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                BtnShowProfile.Enabled = true;
            }
        }

        private void BtnShowProfile_Click(object sender, System.EventArgs e)
        {
            long id = Convert.ToInt64(LblDriverId.Text);
            var qualifications = new List<DriverQualifications>();
            var trainings = new List<DriverTrainings>();
            var qualificationDgv = new List<DriverQualificationsVm>();
            var trainingDgv = new List<DriverTrainingVm>();
            var driver = new Drivers();
            using (Taxi_Driver_SystemEntities db = new Taxi_Driver_SystemEntities())
            {
                qualifications = db.DriverQualifications.Where(r => r.DriverId == id).ToList();
                trainings = db.DriverTrainings.Where(r => r.DriverId == id).ToList();
                driver = db.Drivers.SingleOrDefault(r => r.Id == id);
                for (int i = 0; i < qualifications.Count; i++)
                {
                    var qualificationInsert = new DriverQualificationsVm();
                    qualificationInsert.Id = qualifications[i].Id;
                    qualificationInsert.LicenseNumber = qualifications[i].LicenseNumber;
                    qualificationInsert.QualificationExpiryDate = qualifications[i].QualificationExpiryDate;
                    qualificationInsert.QualificationName = db.Qualifications.SingleOrDefault(r => r.Id == qualifications[i].QualificationId) != null ? db.Qualifications.SingleOrDefault(r => r.Id == qualifications[i].QualificationId).QualificationName : null;
                    qualificationInsert.QualificationType = qualifications[i].QualificationType == 1 ? "License" : "Geographical Tests";
                    qualificationDgv.Insert(i, qualificationInsert);
                }
                for (int j = 0; j < trainings.Count; j++)
                {
                    var trainingInsert = new DriverTrainingVm();
                    trainingInsert.Id = trainings[j].Id;
                    trainingInsert.TrainingExpiryDate = trainings[j].TrainingExpiryDate;
                    long trainingId = trainings[j].TrainingId;
                    trainingInsert.TrainingName = db.Trainings.SingleOrDefault(r => r.Id == trainingId) == null ? db.Trainings.SingleOrDefault(r => r.Id == trainingId).CourseName : null;
                    long? sessionId = trainings[j].DriverSessionTrainingId;
                    var sessions = db.TrainingSessions.SingleOrDefault(r => r.Id == sessionId);
                    trainingInsert.TrainingSessionName = sessions != null ? sessions.SessionName : null;
                    trainingInsert.TrainingSessionStatus = sessions.Status == 1 ? "Pass" : "Fail";
                    trainingDgv.Insert(j, trainingInsert);

                }
                FrnSingleDriverView frmSingleDriverView = new FrnSingleDriverView();
                frmSingleDriverView.DgvQualification.AutoGenerateColumns = false;
                frmSingleDriverView.DgvQualification.DataSource = qualificationDgv;
                frmSingleDriverView.DgvTraining.AutoGenerateColumns = false;
                frmSingleDriverView.DgvTraining.DataSource = trainingDgv;
                if (driver != null)
                {
                    frmSingleDriverView.LblDriverName.Text = driver.FirstName + " " + driver.LastName;
                    frmSingleDriverView.LblDriverDob.Text = driver.Dob.ToString();
                    frmSingleDriverView.LblDriverMobile.Text = driver.Mobile;
                }
                frmSingleDriverView.Show();
            }
        }
    }
}
